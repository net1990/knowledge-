# .NET Core使用容器管理对象，在此我们封装了Autofac用于注入IOC服务
    在KJFrame.Core.Ioc,KJFrame.Core.IocExtensions两个组件中
  
## KJFrame.Core.IocExtensions用于扩展IServiceCollection对象，便于在.NET Core中将KJFrame.Core.Ioc中的三种生命周期的父接口注册到容器中

    KJFrame.Core.Ioc的三个接口
    IScopedService：单次请求中单例(单个请求或者线程中的单例生命周期)
    ISingleService：全局单例，整个程序运行中只有一个实例
    ITransientService：传统实例，每一次获取对象都是一个新的实例（相当于new一个）
   
## Nuget引入程序集

```c#
    Autofac
    Autofac.Extensions.DependencyInjection
```

## 注册服务

    将Startup类中默认public void ConfigureServices(IServiceCollection services)方法做如下修改
    
```c#
        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="services"></param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //此处省略了其他代码
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //此处省略了其他代码
            
            //注册所有IScopedService,ISingleService,ITransientService的实现类
            var current = services.AddBusinessService();
            
            //替换.NET Core的默认容器
            return new AutofacServiceProvider(current);
        }
```
