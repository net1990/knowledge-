# Apollo配置中心

# 1,Apollo简介

    Apollo（阿波罗）是携程框架部门研发的开源配置管理中心，能够集中化管理应用不同环境、不同集群的配置
    ，配置修改后能够实时推送到应用端，并且具备规范的权限、流程治理等特性

    Apollo主要用于解决以下问题：
    在项目中配置文件比较繁杂，而且不同环境的不同配置修改相对频繁，每次发布都需要对应修改配置，如果配置出现错误
    ，需要重新打包发布，时间成本较高，因此需要做统一的配置中心，能做到自动更新配置文件信息，解决以上问题

    Apollo的.Net客户端不依赖任何框架，能够运行于所有.Net运行时环境
    
# 2,Apollo特性

 1)统一管理不同环境、不同集群的配置

    Apollo提供了一个统一界面集中式管理不同环境（environment）、不同集群（cluster）、不同命名空间（namespace）的配置。
 
    同一份代码部署在不同的集群，可以有不同的配置，比如zk的地址等
 
    通过命名空间（namespace）可以很方便的支持多个不同应用共享同一份配置，同时还允许应用对共享的配置进行覆盖

2)配置修改实时生效（热发布）

    用户在Apollo修改完配置并发布后，客户端能实时（1秒）接收到最新的配置，并通知到应用程序
    
 3)版本发布管理

    所有的配置发布都有版本概念，从而可以方便地支持配置的回滚
    
 4)灰度发布

    支持配置的灰度发布，比如点了发布后，只对部分应用实例生效，等观察一段时间没问题后再推给所有应用实例
    
 5)权限管理、发布审核、操作审计

    应用和配置的管理都有完善的权限管理机制，对配置的管理还分为了编辑和发布两个环节，从而减少人为的错误。
    所有的操作都有审计日志，可以方便的追踪问题
    
 6)客户端配置信息监控

    可以在界面上方便地看到配置在被哪些实例使用

# 3,Apollo .net github地址 
    https://github.com/ctripcorp/apollo.net
    
# 4,apollo客户端安装教程（参考https://www.cnblogs.com/a-du/p/8406579.html）
    

# 5,Apollo在.NET Core中的运用

1)安装包

    Com.Ctrip.Framework.Apollo.Configuration
    Tuhu.Extensions.Configuration.ValueBinder.Json

2)服务端引入程序集

```c#
    using Com.Ctrip.Framework.Apollo;
```

3)启动配置 Program.cs
```c#    
    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
    WebHost.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration((hostingContext, builder) =>
    {
        //配置apollo
        builder.AddApollo(builder.Build().GetSection("apollo"))
        
        //添加应用的命名空间
        .AddNamespace("TEST1.Demo0509")
        
        //此默认方法用于添加Application私有命名空间
        .AddDefault();
        
    })
    .UseStartup<Startup>();
```

4)如果您使用Option选项来读取Apollo配置，可以定义Options对象类

>    如下示例：定义了一个简单的Options类

```c#
    public class CachedOptions
    {
        public string Name { get; set; }
        public int RecordeLog { get; set; }
    }
```    

>    如下示例：定义了一个包含自定义节点的Options类

```c#
    public class ComplexOptions
    {
        public string HashName { get; set; }

        public int PIN { get; set; }
        
        //子配置节点
        public UserSetting UserSetting { get; set; }
    }
    
    public class UserSetting
    {
        public string UserType { get; set; }

        public string UserCode { get; set; }
    }
```    
    
5)注册服务配置Startup.cs(如果您仅需要在项目中以字符串的形式读取apollo的配置节点，可以跳过此步骤)

```c#    
    //此处配置主要是用于获取apollo的配置节点，并绑定到指定的Options中
    //您需要安装Tuhu.Extensions.Configuration.ValueBinder.Json包（用于apollo配置 的扩展）
    
    //读取apollo的json配置
    //apollo读取json给对象，需要使用NuGet安装Tuhu.Extensions.Configuration.ValueBinder.Json包
    //语法：services.ConfigureJsonValue<选项类>(Configuration.GetSection("apollo的配置节点名称"));
    //如下
    services.ConfigureJsonValue<CachedOptions>(Configuration.GetSection("CachedOptions"));
    services.ConfigureJsonValue<ComplexOptions>(Configuration.GetSection("ComplexOptions"));
```

 6)在Controller中使用Apollo配置
 
>  以文本方式读取（不读取到对象中去）

```c#  
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultApolloController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public DefaultApolloController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            //apollo读取回来的是一个字符串
            var capConnection = _configuration.GetSection("CapConection")?.Value;
            return capConnection;
        }
    }
```

>    以IOptions<>方式读取,此用法在运行时，只会从配置中读取一次，即不会获取到读取过后的apollo的最新修改

```c#    
    [Route("api/[controller]")]
    [ApiController]
    public class IOptionsDemoController : ControllerBase
    {
        private readonly CachedOptions _options;

        public IOptionsDemoController(IOptions<CachedOptions> options)
        {
            //读取一次的IOptions配置
            _options = options.Value;
        }

        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.Name;
        }
    }
```

>    以IOptionsMonitor方式读取，将会自动刷新，且apollo修改配置发布后，将会自动通知到OnChange事件中

```c#    
    [Route("api/[controller]")]
    [ApiController]
    public class IOptionsMonitorDemoController : ControllerBase
    {
        
        private readonly CachedOptions _options;
        public IOptionsMonitorDemoController(IOptionsMonitor<CachedOptions> options)
        {
            //IOptionsMonitor配置，会自动刷新
            _options = options.CurrentValue;
            
            //apollo修改配置发布后，将会自动通知到此处
            options.OnChange((optionsTemp, n) => {
                var temp = optionsTemp;
                var name = n;
            }
            );
        }
        
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.Name;
        }
    }
```

>    以IOptionsSnapshot方式读取，每一次都是重新计算，重新获取apollo的配置

```c#    
    [Route("api/[controller]")]
    [ApiController]
    public class IOptionsSnapshotDemoController : ControllerBase
    {
        private readonly CachedOptions _options;
        public IOptionsSnapshotDemoController (IOptionsSnapshot<CachedOptions> options)
        {
            //IOptionsSnapshot配置，每一次自动重新计算
            _options = options.Value;
        }

        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.Name;
        }
    }
```

>   这是一个包含自定义节点的示例

```c#
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexOptionsDemoController : ControllerBase
    {
        //ComplexOptions包含了一个UserSetting自定义类型的配置
        //通过apollo的关联类型的覆盖，可以实现某些项目不加载UserSetting配，覆盖项目加载UserSetting配置
        private readonly ComplexOptions _options;
        public ComplexOptionsDemoController(IOptionsSnapshot<ComplexOptions> options)
        {
            //IOptionsSnapshot配置，会自动刷新
            _options = options.Value;
        }

        //关联类型覆盖测试,如下图
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.HashName;
        }
    }
```
> Apollo中的关联配置的覆盖配置功能使用场景
    
    项目A中与项目B中都有HashName，PIN两个配置字段:"ComplexOptions":{"HashName":"","PIN":""}
    项目B中还有 UserType，UserCode两个配置字段:"UserSetting":{"UserType":"","UserCode":""}
    此时可以让项目B的配置关联项目A的配置，并重新覆盖项目A中关于配置："ComplexOptions":{"HashName":"","PIN":"","UserSetting":{"UserType":"","UserCode":""}}
    
![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/releation.png "markdown")

# 6, .Net Core 中IOptionsSnapshot<T>,IOptionsMonitor<T>,IOptions<T>三者的对比
```c#
    //自定义委托配置
    services.Configure<CustomOptions>((option) =>
    {
        option.AppUserID = "小小";
        option.AppUserNo = 10923;
    });
```

    当我们分别以IOptions<CustomOptions>,IOptionsMonitor<CustomOptions>,IOptionsSnapshot<CustomOptions>访问这个CustomOptions配置时
    
    我们会发现IOptions<CustomOptions>只会在第一次读取时，访问这个委托配置的构造函数
    
    IOptionsMonitor<CustomOptions>只会在第一次读取时，访问这个委托配置的构造函数(如果委托配置发生变化时呢?）
    
    IOptionsSnapshot<CustomOptions>则每一次都会访问这个委托配置的构造函数

# 7, .Net Core 命名配置(用于读取命名实例节点)

```json
    //在appsettings.json配置了一个NamedCachedOptionsList节点
    "NamedCachedOptionsList": {
        "TestDemo": {
          "AppUser": "huahua",
          "Users": 12345
        },
        "DevDemo": {
          "AppUser": "jiujiu",
          "Users": 4571
        }
    }
```

```c#
    //在Startup.cs配置
    services.Configure<NamedCachedOptions>("Test1", Configuration.GetSection("NamedCachedOptionsList:TestDemo"));
    services.Configure<NamedCachedOptions>("Test2", Configuration.GetSection("NamedCachedOptionsList:DevDemo"));
```

>使用时可以指定使用哪个实例获取配置选项

```c#
    [Route("api/[controller]")]
    [ApiController]
    public class NamedOptionsDemoController : ControllerBase
    {
        private readonly NamedCachedOptions _testOptions;
        private readonly NamedCachedOptions _devOptions;
        public NamedOptionsDemoController(IOptionsSnapshot<NamedCachedOptions> options)
        {
            //获取命名实例
            //Test1实例读取回来的是TestDemo配置节点{"AppUser": "huahua","Users": 12345}
            _testOptions = options.Get("Test1");
            
            //Test2实例读取回来的是DevDemo配置节点{"AppUser": "jiujiu","Users": 4571}
            _devOptions = options.Get("Test2");
        }

        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _testOptions?.AppUser;
        }
    }
```