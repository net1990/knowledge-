﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KJFrame.Core.DemoProtocol;
using KJFrame.Core.DemoProtocol.Response;
using Microsoft.AspNetCore.Mvc;

namespace KJFrame.Core.ClientApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        public ValuesController(IUserService userService, IRoleService roleService)
        {
            this._userService = userService;
            this._roleService = roleService;
        }

        [HttpGet]
        public string Index()
        {
            return "Hello Word!";
        }

        [HttpGet]
        public ActionResult<BaseResponse> Get(int id)
        {
            //客户端为5002(UseUrls("http://localhost:5002"))
            //此处请求的是服务端5001的数据
            return _userService.GetBill(new DemoProtocol.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
        }

        [HttpGet]
        public ActionResult<String> GetString(int id)
        {
            return _userService.GetString(new DemoProtocol.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
        }

        [HttpGet]
        public ActionResult<BaseResponse> GetRole(int id)
        {
            //客户端为5002(UseUrls("http://localhost:5002"))
            //此处请求的是服务端5001的数据
            return _roleService.GetRole(new DemoProtocol.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
        }
    }
}
