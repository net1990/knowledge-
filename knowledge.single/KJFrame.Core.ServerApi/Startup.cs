﻿using KJFrame.Core.DemoBLL;
using KJFrame.Core.DemoProtocol;
using KJFrame.Core.Rpc.Protocol;
using KJFrame.Core.Rpc.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace KJFrame.Core.ServerApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton<IRpcServiceTypeFinder, FakeRpcServiceTypeFinder>();
            services.AddRpcServer(option => {
                Configuration.GetSection(RpcConfigInformation.RpcServerConfigSectionName).Bind(option);
            });
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRoleService, RoleService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }

    public class FakeRpcServiceTypeFinder : IRpcServiceTypeFinder
    {
        public List<Type> FindAllRpcServiceType()
        {
            return new List<Type> {
                typeof(IUserService)
            };
        }
    }
}
