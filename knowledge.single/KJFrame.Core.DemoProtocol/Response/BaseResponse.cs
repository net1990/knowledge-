﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace KJFrame.Core.DemoProtocol.Response
{
    [ProtoContract]
   public  class BaseResponse
    {
        /// <summary>
        /// 状态
        /// </summary>
        [ProtoMember(1)]
        public bool Status { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        [ProtoMember(2)]
        public string Message { get; set; }
    }
}
