﻿using KJFrame.Core.DemoProtocol.Request;
using KJFrame.Core.DemoProtocol.Response;
using KJFrame.Core.Rpc.Protocol;
using KJFrame.Core.Rpc.Protocol.ServiceAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KJFrame.Core.DemoProtocol
{
    [RpcService]
    public interface IRoleService : IRpcService
    {
        [RpcMethod]
        Task<BaseResponse> GetRole(IdRequest request, CancellationToken token = default);
    }
}
