﻿using KJFrame.Core.DemoProtocol.Request;
using KJFrame.Core.DemoProtocol.Response;
using KJFrame.Core.Rpc.Protocol;
using KJFrame.Core.Rpc.Protocol.ServiceAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KJFrame.Core.DemoProtocol
{
    [RpcService]
    public interface IUserService:IRpcService
    {
        //Rpc方法的请求参数，返回参数具有class约束
        //Rpc方法类似于：Task<TOut> Fun<TOut>(TIn request CancellationToken token=default) where TRequest : class where TResponse : class
        //所以Rpc方法的请求桉树，返回参数可以是自定义类，也可以是string等类，但不可以是int,bool,float等数据结构

        [RpcMethod]
        Task<BaseResponse> GetBill(IdRequest request, CancellationToken token=default );

        //这是一个返回类型为String的Rpc方法
        [RpcMethod]
        Task<string> GetString(IdRequest request, CancellationToken token = default);

    }
}
