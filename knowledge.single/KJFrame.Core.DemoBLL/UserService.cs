﻿using KJFrame.Core.DemoProtocol;
using KJFrame.Core.DemoProtocol.Request;
using KJFrame.Core.DemoProtocol.Response;
using KJFrame.Core.Rpc.Server;
using System.Threading;
using System.Threading.Tasks;

namespace KJFrame.Core.DemoBLL
{
    public class UserService : RpcServiceBase, IUserService
    {
        public Task<BaseResponse> GetBill(IdRequest request, CancellationToken token = default)
        {
            return Task.FromResult(new BaseResponse() { Status=true,Message="50002pp+No"+request.Id });
        }

        public Task<string> GetString(IdRequest request, CancellationToken token = default)
        {
            return Task.FromResult(request.Id.ToString());
        }

    }
}
