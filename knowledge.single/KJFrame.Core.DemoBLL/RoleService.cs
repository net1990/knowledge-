﻿using KJFrame.Core.DemoProtocol;
using KJFrame.Core.DemoProtocol.Request;
using KJFrame.Core.DemoProtocol.Response;
using KJFrame.Core.Rpc.Server;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;

namespace KJFrame.Core.DemoBLL
{
    public class RoleService : RpcServiceBase, IRoleService
    {
        public Task<BaseResponse> GetRole(IdRequest request, CancellationToken token = default)
        {
            var context = new
            {
                //过期时间：9999-12-31T23:59:59.9999999
                Deadline = Context.Deadline,
                //服务器：127.0.0.1:12003
                Host = Context.Host,
                //请求的路径： /IRoleService/GetRole
                Method = Context.Method,
                //发起请求的客户端：ipv4:127.0.0.1:54792
                Peer = Context.Peer,
                //请求头：[{"Key":"user-agent",
                //             "ValueBytes":"Z3JwYy1jc2hhcnAvMS4yMS4wIGdycGMtYy83LjAuMCAod2luZG93czsgY2h0dHAyOyBnYW5kYWxmKQ==",
                //            "Value":"grpc-csharp/1.21.0 grpc-c/7.0.0 (windows; chttp2; gandalf)","IsBinary":false}]
                RequestHeaders = Context.RequestHeaders,
                //状态：{"StatusCode":0,"Detail":""}
                Status = Context.Status,
                Trailers = Context.ResponseTrailers,
                UserState = Context.UserState,
                GetRole = request.Id
            };
            return Task.FromResult(new BaseResponse() { Status = false, Message = JsonConvert.SerializeObject(context) });
        }
    }
}
