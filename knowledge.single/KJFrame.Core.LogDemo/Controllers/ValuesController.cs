﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.Log;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KJFrame.Core.LogDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ILogger _logger;

        public ValuesController(ILogger logger)
        {
            this._logger = logger;
        }

        // GET api/values
        [HttpGet("~/index")]
        public ActionResult<string > Index()
        {
            _logger.LogKJTrace(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.LogKJDebug(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.LogKJInformation(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.LogKJWarning(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.LogKJError(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.LogKJCritical(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            return "hello word";
        }

    }
}
