﻿using DotNetCore.CAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KJFrame.Core.CapDemo.SubscribeService
{
    public class CapUserService : ICapSubscribe, ICapUserService
    {
        /// <summary>
        /// 使用ICapSubscribe订阅
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.test5",Group = "transaction")]
        public void SubscribeWithnoController(string date)
        {
            Console.WriteLine($"SubscribeWithnoController接收到订阅topic为kjframe.test5，组为transaction:{date}");
        }
    }

    public interface ICapUserService
    {
        void SubscribeWithnoController(string date);
    }
}
