﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;

namespace KJFrame.Core.CapDemo.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CapDemoController : Controller
    {
        private readonly ICapPublisher _capBus;

        //相同的订阅组订阅同一个topic时，只会被一个订阅者消费
        // 同一个topic可以被多个不同的订阅组订阅，如下四个不同的订阅者分别订阅指定group或以默认group的方式订阅了同一个topic，当producer发送消息来时，Received表中会给每一个订阅者插入一条Content字段相同的订阅记录
        //Id    Name            Group                               Content                                                                                                                                                                                                                                                  Added                              
        //16	 kjframe.test    group1.v1                                                 { "Id":"5cfdf02ded40720ed4e98de9","Timestamp":"2019-06-10T13:52:45.4107162+08:00","Content":"2019-06-10 13:52:45","CallbackName":null}	   2019-06-10 13:52:47.4930000	
        //24	 kjframe.test    ICapSubscribeGroup.v1                             { "Id":"5cfdf02ded40720ed4e98de9","Timestamp":"2019-06-10T13:52:45.4107162+08:00","Content":"2019-06-10 13:52:45","CallbackName":null}   2019-06-10 13:52:47.4970000	
        //25	 kjframe.test    cap.queue.kjframe.core.capdemo.v1          { "Id":"5cfdf02ded40720ed4e98de9","Timestamp":"2019-06-10T13:52:45.4107162+08:00","Content":"2019-06-10 13:52:45","CallbackName":null}   2019-06-10 13:52:47.4970000	
        //28	 kjframe.test    WDB.v1                                                     { "Id":"5cfdf02ded40720ed4e98de9","Timestamp":"2019-06-10T13:52:45.4107162+08:00","Content":"2019-06-10 13:52:45","CallbackName":null}   2019-06-10 13:52:47.4970000	

        public CapDemoController(ICapPublisher capPublisher)
        {
            _capBus = capPublisher;
        }

        public string Index()
        {
            return "hello world";
        }


        /// <summary>
        /// 生产消息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DemoPublish()
        {
            await _capBus.PublishAsync("kjframe.test3", DateTime.Now);
            return Content("Hello word");
        }

        /// <summary>
        /// 不指定订阅组，特定格式参数订阅
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.test3")]
        [NonAction]
        public void DemoSubscribe1(DateTime date)
        {
            Console.WriteLine($"kjframe.test3接收到订阅:{date.ToString("yyyy-MM-dd hh:mm:ss")}");
        }

        /// <summary>
        /// 指定订阅组
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.test3", Group = "group1")]
        [NonAction]
        public void DemoSubscribe2(string date)
        {
            Console.WriteLine($"group1接收到消息:{date}");
        }

        /// <summary>
        /// 指定订阅组
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.test3", Group = "group2")]
        [NonAction]
        public void DemoSubscribe3(string date)
        {
            Console.WriteLine($"group2接收到消息:{date}");
        }

    }
}
