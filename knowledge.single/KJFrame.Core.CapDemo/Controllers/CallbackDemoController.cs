﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;

namespace KJFrame.Core.CapDemo.Controllers
{
    /// <summary>
    /// 测试回调
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CallbackDemoController : Controller
    {
        private readonly ICapPublisher _capBus;

        public CallbackDemoController(ICapPublisher capPublisher)
        {
            _capBus = capPublisher;
        }

        /// <summary>
        /// 服务端：带callbackName参数的推送
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DemoPublishCallback()
        {
            await _capBus.PublishAsync("kjframe.test4", DateTime.Now, "topic.callback");
            return Content("服务端这个推送了一个包含callbackName=FailCallBack的消息");
        }

        /// <summary>
        /// 客户端订阅，这个订阅具有返回值，它订阅了一个content中callbackName有值的topic
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [CapSubscribe("kjframe.test4", Group = "callclient")]
        [NonAction]
        public DateTime DemoSubscribeCallbackClient(DateTime date)
        {
            Console.WriteLine($"callclient已处理，请回调:{date.ToString("yyyy-MM-dd hh:mm:ss")}");
            return DateTime.Now.AddDays(10);
        }

        /// <summary>
        /// 服务的那订阅，这个订阅的topic的值是TestCallback方法中的callbackName的值
        /// 即这个方法用来处理客户端订阅方法返回来的回调
        /// </summary>
        /// <param name="message">是客户端订阅方法的返回值，如这里的DateTime SubscribeCallback(DateTime date)返回值</param>
        [CapSubscribe("topic.callback", Group = "callserver")]
        [NonAction]
        public void DemoSubscribeCallbackServer(string message)
        {
            Console.WriteLine($"topic.callback接收到来自客户端的回调:{message}");
        }

    }
}
