﻿using KJFrame.Core.Job;
using System;
using System.Collections.Generic;
using System.Text;

namespace KJFrame.Core.HangfireDemo
{
    public class OrderTask : IRepeatJob
    {
        public OrderTask()
        {
            JobName = "OrderTask";
        }
        public string JobName
        {
            get; set;
        }

        public void Excute()
        {
            Console.WriteLine($"执行任务{JobName}:{DateTime.Now}");
        }
    }
}
