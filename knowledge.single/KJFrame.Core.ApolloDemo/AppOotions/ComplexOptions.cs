﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KJFrame.Core.ApolloDemo.AppOotions
{
    public class ComplexOptions
    {
        public string HashName { get; set; }

        public int PIN { get; set; }

        public UserSetting UserSetting { get; set; }
    }
}
