﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KJFrame.Core.ApolloDemo.AppOotions
{
    public class UserSetting
    {
        public string UserType { get; set; }

        public string UserCode { get; set; }
    }
}
