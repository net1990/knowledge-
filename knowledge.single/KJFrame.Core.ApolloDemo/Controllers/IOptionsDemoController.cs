﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.ApolloDemo.AppOotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace KJFrame.Core.ApolloDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IOptionsDemoController : ControllerBase
    {
        private readonly CachedOptions _options;

        public IOptionsDemoController(IOptions<CachedOptions> options)
        {
            //读取一次的IOptions配置
            _options = options.Value;
        }

        /// <summary>
        /// 默认方式
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.Name;
        }
    }
}