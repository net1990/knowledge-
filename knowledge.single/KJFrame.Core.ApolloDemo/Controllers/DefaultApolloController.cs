﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace KJFrame.Core.ApolloDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultApolloController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public DefaultApolloController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 默认方式
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            var capConnection = _configuration.GetSection("CapConection")?.Value;
            return capConnection;
        }
    }
}