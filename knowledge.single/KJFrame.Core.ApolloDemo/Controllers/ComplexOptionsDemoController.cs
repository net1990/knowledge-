﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.ApolloDemo.AppOotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace KJFrame.Core.ApolloDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexOptionsDemoController : ControllerBase
    {
        /// <summary>
        /// ComplexOptions包含了一个UserSetting自定义类型的配置，通过apollo的关联类型的覆盖，可以实现某些项目不加载UserSetting配，覆盖项目加载UserSetting配置
        /// </summary>
        private readonly ComplexOptions _options;
        public ComplexOptionsDemoController(IOptionsSnapshot<ComplexOptions> options)
        {
            //IOptionsSnapshot配置，会自动刷新
            _options = options.Value;
        }

        /// <summary>
        /// 关联类型覆盖测试
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.HashName;
        }
    }
}