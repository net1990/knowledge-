﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.ApolloDemo.AppOotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace KJFrame.Core.ApolloDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NamedOptionsDemoController : ControllerBase
    {
        private readonly NamedCachedOptions _testOptions;
        private readonly NamedCachedOptions _devOptions;
        public NamedOptionsDemoController(IOptionsSnapshot<NamedCachedOptions> options)
        {
            //命名实例配置
            _testOptions = options.Get("Test1");
            _devOptions = options.Get("Test2");
        }

        /// <summary>
        /// 默认方式
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _testOptions?.AppUser;
        }
    }
}