﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.ApolloDemo.AppOotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace KJFrame.Core.ApolloDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SampleIOptionsMonitorController : ControllerBase
    {
        
        private readonly CustomOptions _options;
        public SampleIOptionsMonitorController(IOptionsMonitor<CustomOptions> options)
        {
            //IOptionsMonitor配置，会自动刷新
            _options = options.CurrentValue;
            //apollo修改配置发布后，将会自动通知到此处
            options.OnChange((optionsTemp, n) => {
                var temp = optionsTemp;
                var name = n;
            }
            );
        }

        /// <summary>
        /// 默认方式
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.AppUserID;
        }
    }
}