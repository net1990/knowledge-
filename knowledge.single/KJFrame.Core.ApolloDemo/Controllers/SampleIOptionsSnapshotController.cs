﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.ApolloDemo.AppOotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace KJFrame.Core.ApolloDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SampleIOptionsSnapshotController : ControllerBase
    {
        private readonly CustomOptions _options;
        public SampleIOptionsSnapshotController(IOptionsSnapshot<CustomOptions> options)
        {
            //IOptionsSnapshot配置，会自动刷新
            _options = options.Value;
        }

        /// <summary>
        /// 默认方式
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetDemo()
        {
            return _options?.AppUserID;
        }
    }
}