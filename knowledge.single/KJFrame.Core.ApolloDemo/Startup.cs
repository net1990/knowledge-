﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KJFrame.Core.ApolloDemo.AppOotions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace KJFrame.Core.ApolloDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "apollo demo", Version = "v1" });
            });

            #region 读取apollo的json配置
            //读取apollo的json配置
            //apollo读取json给对象，需要使用NuGet安装Tuhu.Extensions.Configuration.ValueBinder.Json包
            services.ConfigureJsonValue<CachedOptions>(Configuration.GetSection("CachedOptions"));
            services.ConfigureJsonValue<ComplexOptions>(Configuration.GetSection("ComplexOptions"));
            #endregion
            #region 自定义构造函数配置
            //自定义委托配置
            services.Configure<CustomOptions>((option) =>
            {
                option.AppUserID = "小小";
                option.AppUserNo = 10923;
            });
            #endregion
            #region 读josn文件的命名配置
            services.Configure<NamedCachedOptions>("Test1", Configuration.GetSection("NamedCachedOptionsList:TestDemo"));
            services.Configure<NamedCachedOptions>("Test2", Configuration.GetSection("NamedCachedOptionsList:DevDemo"));
            #endregion
        }

        /// <summary>
        /// 配置管道
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //启用中间件服务生成Swagger作为JSON终结点
            app.UseSwagger();
            //启用中间件服务对swagger-ui，指定Swagger JSON终结点
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "apollo demo api");
            });
            app.UseMvc();
        }
    }
}
