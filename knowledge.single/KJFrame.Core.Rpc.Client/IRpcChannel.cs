using System.Threading;
using System.Threading.Tasks;

namespace KJFrame.Core.Rpc.Client
{
    public interface IRpcChannel
    {
         Task<TResponse> CallUnaryMethodAsync<TRequest, TResponse>(TRequest request, string serviceName, string methodName, CancellationToken token)
            where TRequest : class
            where TResponse : class;
    }
}
