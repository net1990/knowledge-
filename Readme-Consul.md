##API网关与发现##

###参考网站###

  1.山猫的官方文档：https://ocelot.readthedocs.io

  2.腾飞的中文文档：https://www.cnblogs.com/jesse2013/p/net-core-apigateway-ocelot-docs.html

  3.consul的官网：https://www.consul.io/

###简单介绍###
	API网关—— 它是系统的暴露在外部的一个访问入口。这个有点像代理访问，就像一个公司的门卫承担着寻址、限制进入、安全检查、位置引导等功能。



###常用网关Ocelot基本功能###

	Ocelot是一个用.NET Core实现并且开源的API网关，它功能强大，包括了：路由、请求聚合、服务发现、认证、鉴权、限流熔断、
	并内置了负载均衡器与Service Fabric(分布式系统平台)、Butterfly Tracing(分布式跟踪APM)集成
    
    Butterfly Tracin作者已经不再维护了，可集入SkyWalking.

###Ocelot基本原理###

	Ocelot是一堆的asp.net core middleware组成的一个管道，当它拿到请求之后会用一个request builder来构造一个
	HttpRequestMessage发到下游的真实服务器，等下游的服务返回response之后再一个middleware将它返回的
	HttpResponseMessage映射到HttpResponse上。


###Ocelot常用功能###

 	1.基本集成:包含路由功能，请求聚合

 	2.集成IdentityServer Or Jwt（认证和鉴权):
    
    3.网关集群配置（高可用，需新增负载均衡器，比如Ngix）

    4.结合Consul做服务发现和健康检查

###基本配置###
	{
	    "ReRoutes": [],
	    "GlobalConfiguration":
	    {
	        "BaseUrl": "https://localhost"
	    }
	}
###多钟方式限流###

	1.ReRoutes某一请求中配置限流
 	"RateLimitOptions": 
    {                  //限流
       "ClientWhitelist": [],               //白名单
       "EnableRateLimiting": false,         //是否启用限流
       "Period": "",                        //统计时间段：1s, 5m, 1h, 1d
       "PeriodTimespan": 0,                 //多少秒之后客户端可以重试
       "Limit": 0                           //在统计时间段内允许的最大请求数量
    }
    2.GlobalConfiguration配置限流
    "RateLimitOptions":
    {
  	   "DisableRateLimitHeaders": false,           //Http头  X-Rate-Limit 和 Retry-After 是否禁用
  	   "QuotaExceededMessage": "Customize Tips!",  //当请求过载被截断时返回的消息
  	   "HttpStatusCode": 999,                      //当请求过载被截断时返回的http status
  	   "ClientIdHeader" : "Test"                   //用来识别客户端的请求头，默认是 ClientId
	}

          
###配置解释###

        {
            "ReRoutes": [
            {
             //Upstream表示上游请求，即客户端请求到API Gateway的请求
              "UpstreamPathTemplate": "/", 			 //请求路径模板
              "UpstreamHttpMethod": [ 				 //请求方法数组
                "Get"
              ],
        
              										//Downstreamb表示下游请求，即API Gateway转发的目标服务地址
              "DownstreamScheme": "http", 			//请求协议，目前应该是支持http和https
              "DownstreamHost": "localhost", 		//请求服务地址，应该是可以是IP及域名
              "DownstreamPort": 51779, 				//端口号
              "DownstreamPathTemplate": "/", 		//下游请求地址模板
              "RouteClaimsRequirement": { 			//标记该路由是否需要认证
                "UserType": "registered"            //示例,K/V形式，授权声明，授权token中会包含一些claim
													//，如填写则会判断是否和token中的一致，不一致则不准访问
              },
              									    //以下三个是将access claims转为用户的Header Claims,QueryString，该功能只有认证后可用
              "AddHeadersToRequest": { //
                "UserType": "Claims[sub] > value[0] > |", //示例
                "UserId": "Claims[sub] > value[1] > |"//示例
              },
              "AddClaimsToRequest": {},
              "AddQueriesToRequest": {},
        
              "RequestIdKey": "",                     //设置客户端的请求标识key，此key在请求header中，会转发到下游请求中
              "FileCacheOptions": {                   //缓存设置
                "TtlSeconds": 15,                     //ttl秒被设置为15，这意味着缓存将在15秒后过期。
                "Region": "" 						  //缓存region，可以使用administrator API清除
              },
              "ReRouteIsCaseSensitive": false,        //路由是否匹配大小写
              "ServiceName": "",					  //服务名称，服务发现时必填
              "QoSOptions": {           			  //断路器配置,目前Ocelot使用的Polly
                "ExceptionsAllowedBeforeBreaking": 0, //打开断路器之前允许的例外数量。
                "DurationOfBreak": 0,                 //断路器复位之前，打开的时间(毫秒)
                "TimeoutValue": 0                     //请求超时时间(毫秒)
              },
              "LoadBalancer": "", //负载均衡 RoundRobin(轮询)/LeastConnection(最少连接数)/NoLoadBalance (总是发往第一个请求或者是服务发现)
              "RateLimitOptions": { 				 //限流
                "ClientWhitelist": [],				 //白名单
                "EnableRateLimiting": false,		 //是否启用限流
                "Period": "",						 //统计时间段：1s, 5m, 1h, 1d
                "PeriodTimespan": 0,				 //多少秒之后客户端可以重试
                "Limit": 0							 //在统计时间段内允许的最大请求数量
              },
                "AuthenticationOptions": {           //认证配置
                "AuthenticationProviderKey": "",     //这个key对应的是代码中.AddJWTBreark中的Key
                "AllowedScopes": []					 //使用范围
              },
              "HttpHandlerOptions": {
                "AllowAutoRedirect": true, 			//指示请求是否应该遵循重定向响应。 
										   			//如果请求应该自动遵循来自Downstream资源的重定向响应，
										   			//则将其设置为true; 否则为假。 默认值是true。
                "UseCookieContainer": true 			//该值指示处理程序是否使用CookieContainer属性来存储服务器Cookie，
													//并在发送请求时使用这些Cookie。 默认值是true。
              },
              "UseServiceDiscovery": false 		    //使用服务发现，目前Ocelot只支持Consul的服务发现
            }
            ],
            "GlobalConfiguration": {
				 "BaseUrl": "https://localhost"
				 "RequestIdKey": "OcRequestId",			
    			"ServiceDiscoveryProvider": {
            	"Host": "localhost",
            	"Port": 19081,
            	"Type": "ServiceFabric"
        	  }
		    }
        }

    

###Consul服务注册与发现###

作用：用于容灾，实时监控服务状态

基本介绍：

	Consul是一种服务网格解决方案，提供具有服务发现，配置和分段功能的全功能控制平面。这些功能中的每一个都可以根据需要单独使用，
	也可以一起使用以构建全服务网格。Consul需要数据平面并支持代理和本机集成模型。Consul附带一个简单的内置代理，因此一切都
	可以开箱即用，但也支持第三方代理集成，如Envoy

    健康检查的类型分为好几种，常用的有：script检查，通过执行一段脚本检查，功能最强大，但是要手写脚本；http检查，
	通过发起http Get请求检查；docker检查，检查docker容器；TCP检查，检查TCP是否连接；gRPC检查，通过gRPC的健康协议进行检查

Consul的主要特点：
	
	服务发现: Consul的客户端可用提供一个服务,比如 api 或者mysql ,另外一些客户端可用使用Consul去发现一个指定服务的提供者.
				 
			  通过DNS或者HTTP应用程序可用很容易的找到他所依赖的服务.

	健康检查：Consul客户端可用提供任意数量的健康检查,指定一个服务(比如:webserver是否返回了200 OK 状态码)或者使用本地节点
			(比如:内存使用是否大于90%).这个信息可由operator用来监视集群的健康.被服务发现组件用来避免将流量发送到不健康的主机.

	Key/Value存储：应用程序可用根据自己的需要使用Consul的层级的Key/Value存储.比如动态配置,功能标记,协调,领袖选举等等,
			 简单的HTTP API让他更易于使用.

	多数据中心：Consul支持开箱即用的多数据中心.这意味着用户不需要担心需要建立额外的抽象层让业务扩展到多个区域. 
			Consul面向DevOps和应用开发者友好.是他适合现代的弹性的基础设施.


Consul基本使用：

	1.下载安装Consul
    
    2.若未配置环境变量(命令行中cd到当前目录)

    3.基本命令：consul agent -dev，打开UI：http://localhost:8500
    
    4.集群成员：consul members
    
    5.视图查看： curl localhost:8500/v1/catalog/nodes
    
   
命令常用选项：

    consul agent 命令的常用选项，如下：

	-data-dir

	作用：指定agent储存状态的数据目录

	这是所有agent都必须的

	对于server尤其重要，因为他们必须持久化集群的状态

	-config-dir

	作用：指定service的配置文件和检查定义所在的位置
	通常会指定为”某一个路径/consul.d”（通常情况下，.d表示一系列配置文件存放的目录）

	-config-file

	作用：指定一个要装载的配置文件
	该选项可以配置多次，进而配置多个配置文件（后边的会合并前边的，相同的值覆盖）

	-dev

	作用：创建一个开发环境下的server节点
	该参数配置下，不会有任何持久化操作，即不会有任何数据写入到磁盘
	这种模式不能用于生产环境（因为第二条）

	-bootstrap-expect

	 作用：该命令通知consul server我们现在准备加入的server节点个数，该参数是为了延迟日志复制的启动直到我们指定数量的server节点成功的加入后启动。

    -node

	作用：指定节点在集群中的名称
	该名称在集群中必须是唯一的（默认采用机器的host）
	推荐：直接采用机器的IP

	-bind
	
	作用：指明节点的IP地址
	有时候不指定绑定IP，会报Failed to get advertise address: Multiple private IPs found. Please configure one. 的异常
	
	-server
	
	作用：指定节点为server
	每个数据中心（DC）的server数推荐至少为1，至多为5
	所有的server都采用raft一致性算法来确保事务的一致性和线性化，事务修改了集群的状态，且集群的状态保存在每一台server上保证可用性
	server也是与其他DC交互的门面（gateway）
	
	-client
	
	作用：指定节点为client，指定客户端接口的绑定地址，包括：HTTP、DNS、RPC
	默认是127.0.0.1，只允许回环接口访问
	若不指定为-server，其实就是-client
	
	-join
	
	作用：将节点加入到集群
	
	-datacenter（老版本叫-dc，-dc已经失效）
	
	作用：指定机器加入到哪一个数据中心中
	
	-client 
	参数可指定允许客户端使用什么ip去访问，例如-client 192.168.11.143 表示可以使用http://192.168.11.143:8500/ui 去访问。

例如：

   	加入集群：consul agent -server -ui -bootstrap -data-dir=/tmp/consul -node=consul-server-2
		    -client=客户端IP -bind=服务端IP -datacenter=dc1 -join 集群IP


consul具体使用：

	 1.appsettings.json配置：
        "Consul": 
		{
    	  "IP": "127.0.0.1",
          "Port": "8500"
        }

	 2.在服务管道中进行注册:
	  public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            ServiceEntity serviceEntity = new ServiceEntity
            {
                IP = "127.0.0.1",
                Port = Convert.ToInt32(Configuration["Service:Port"]),
                ServiceName = Configuration["Service:Name"],
                ConsulIP = Configuration["Consul:IP"],
                ConsulPort = Convert.ToInt32(Configuration["Consul:Port"])
            };
            app.RegisterConsul(lifetime, serviceEntity);
            app.UseMvc();
        }

