# .NET Core的IIS部署教程

# 教程前提
  
  Windows10 64位操作系统
  .Net Core 2.2
  
# 部署在IIS中时，请注意Programe中代码使用UseIIS()

# 安装.NET Core运行时环境(此处安装的位2.2.5)

1，在IIS中打开模块查看是否已安装AspNetCoreModuleV2,或者直接在cmd使用dotnet --info 命令查看已安装的NET Core Runtime

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/IIS4.png "markdown")

2，如果没有安装【对应版本】的.NET Core Runtime,前往官方地址下载.net core Runtime :https://dotnet.microsoft.com/download

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/IIS1.png "markdown")

3，以管理员省份安装下载下来的程序（本例文件名为dotnet-hosting-2.2.5-win.exe）

4，安装完后，在cmd命令中输入dotnet --info回车，可以看到.NET Core Runtime installed列表下看到安装好的运行时环境

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/IIS2.png "markdown")

# 打包部署

1，发布打包程序（打包过程请参考CentOS教程中的【打包过程】）到自己定义的部署目录下（任意命名，选文件夹，如D:/www/agent）

2，创建IIS站点，将站点的物理路径映射到部署目录(示例D:/www/agent)

3，修改应用程序池 选择无托管代码

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/IIS3.png "markdown")

4，启动网站


