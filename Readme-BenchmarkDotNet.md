# .NET Core使用BenchmarkDotNet进行性能基准测试
    用于性能测试；但是注意请勿因为使用该测试工具而导致在项目中的性能与开发效率，功能选择之间的误区

   
## Nuget引入程序集

```c#
    BenchmarkDotNet
```

## 基本性能测试

```c#
        public class SingleVsFirst
        {
            private readonly List<string> _haystack = new List<string>();
            private readonly int _haystackSize = 1000000;
            private readonly string _needle = "needle";

            public SingleVsFirst()
            {
                Enumerable.Range(1, _haystackSize).ToList()
				.ForEach(x => _haystack.Add(x.ToString()));
				
                _haystack.Insert(_haystackSize / 2, _needle);
            }

			//使用Benchmark标记为测试方法
            [Benchmark]
            public string Single() => _haystack.SingleOrDefault(x => x == _needle);

			//使用Benchmark标记为测试方法
            [Benchmark]
            public string First() => _haystack.FirstOrDefault(x => x == _needle);

        }
		
		//使用控制台调用测试BenchmarkRunner测试
		public class Program
		{
			public static void Main(string[] args)
			{
				//BenchmarkRunner启动需要测试的类
				var summary = BenchmarkRunner.Run<SingleVsFirst>();
				Console.ReadLine();
			}
		}
		
		//测试结果
		//| Method |     Mean |    Error |   StdDev |
        //|------- |---------:|---------:|---------:|
        //| Single | 262.4 ns | 1.845 ns | 1.726 ns |
        //|  First | 167.9 ns | 1.974 ns | 1.847 ns |
```

## 内存性能测试

```c#

	//标记使用内存
    [MemoryDiagnoser]
    public class Benchmarking
    {
		//标记测试，注意返回值不能直接为IEnumerable<int>或者IQuery<int>之类的延迟加载项
        [Benchmark]
        public List<int> GetList()
        {
            List<int> list = new List<int>();
            for (int i = 0; i < 100000; i++)
            {
                list.Add(i);
            }
            List<int> list1 = new List<int>();
            foreach (var item in list)
            {
                list1.Add(item);
            }
            return list1;
        }

		//标记测试
        [Benchmark]
        public List<int> GetArrayList()
        {
            ArrayList list = new ArrayList();
            for (int i = 0; i < 100000; i++)
            {
                list.Add(i);
            }
            List<int> list1 = new List<int>();
            foreach (var item in list)
            {
                list1.Add((int)item);
            }
            return list1;
        }
    }
	
	//测试结果,从结果我们可以看出ArrayList使用拆装箱对性能的影响，时间和内存的开销更大
	//|       Method |       Mean |     Error |    StdDev | Ratio | RatioSD |     Gen 0 |     Gen 1 |    Gen 2 | Allocated |
	//|------------- |-----------:|----------:|----------:|------:|--------:|----------:|----------:|---------:|----------:|
	//|      GetList |   967.9 us |  19.17 us |  19.68 us |  1.00 |    0.00 |  582.0313 |  501.9531 | 501.9531 |      2 MB |
	//| GetArrayList | 6,328.6 us | 121.76 us | 135.34 us |  6.55 |    0.23 | 1117.1875 | 1101.5625 | 726.5625 |   5.29 MB |
```
    
