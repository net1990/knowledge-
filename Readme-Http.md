# Http请求

##  引
    
    我们没有再去封装传统的HttpHelper帮助类，而是在成熟的Http请求工具RestSharp的基础上封装了一个KJFrame.Core.HttpHelp
    
    由于当前最新版本的RestSharp（106.6.9）使用的是其内置的Json序列化工具，其针对Array[]等返回类型时，序列化会出问题，因此我们在封装的组件中
    添加了Newtonsoft.Json的序列化的HTTP方法(以Advanced结尾的方法)
    
    RestSharp计划要在下一个大版本107采用Newtonsoft.Json来替换它内置的序列化工具
    
    KJFrame.Core.HttpHelp依赖KJFrame.Core.ITraceLog(参考日志教程),该组件会将每一个Http的请求报文，返回报文，异常输出到日志
    
## 引入程序集KJFrame.Core.HttpHelp

```c#
    KJFrame.Core.HttpHelp
```

## 注册服务（参考Autofac的容器教程)

由于KJFrame.Core.HttpHelp的IRestClientService继承了IScopedService
因此在Startup中，我们的KJFrame.Core.IocExtensions的AddBusinessService方法会自动将IRestClientService注册进来


## 调用
```c#
public class TestService : IScopedService
{
    private readonly IRestClientService _restClientService;
    
    public TestService(IRestClientService restClientService)
    {
        this._restClientService = restClientService;
    }
    
    /// <summary>
    /// Post ([FromBody]AppDTO dto)
    /// </summary>
    /// <returns></returns>
    public AppDTO PostTest()
    {
        AppDTO param = new AppDTO()
        {
            UserName = "Post body",
            Age = 1
        };
        var response = _restClientService.PostJson<AppDTO>("http://localhost:5009/api/RestApi/PostAppBody", param);
        return response;
    }
    
    /// <summary>
    /// Post ([FromQuery]AppDTO dto)
    /// </summary>
    /// <returns></returns>
    public AppDTO PostApp7()
    {
        var response = _restClientService.PostQuery<AppDTO>("http://localhost:5009/api/RestApi/PostAppUrl",
        new Dictionary<string, string>() { { "UserName", "高级传参" },{ "Age","12" } });
        
        return response;
    }
}
```

### 关于TraceID，如果您调用处的类是使用IScopedService单次请求单例时，不传TraceID
    如果您调用处的类使用ISingleService注册，您可以传TraceID实现日志跟踪

### 请求的接口为HttpPost,参数[FormBody]时
    1，使用PostJsonAdvanced<T>(string url, object request)发起请求，request需要是可序列化为Json的对象
    返回类型的成员支持Array等类型
    
    2，使用PostJson<T>(string url, object request)发起请求，request需要是可序列化为Json的对象
    返回类型的成员只支持Primitives, Decimal,DateTime,String,Guid,List,Dictionary<T1, T2>,Nested classes,Nullable
    
    3，使用Post(string url, object request)发起请求，request需要是可序列化为Json的对象
    直接返回string类型的原报文
    
### 请求的接口为HttpPost,参数[FormQuery]或者类似(string userName,int age)格式时

    1，使用PostQueryAdvanced<T>(string url, Dictionary<string, string> request)发起请求，Request为键值对
    返回类型的成员支持Array等类型
    
    2，使用PostQuery<T>(string url, Dictionary<string, string> request)发起请求，Request为键值对
    返回类型的成员只支持Primitives, Decimal,DateTime,String,Guid,List,Dictionary<T1, T2>,Nested classes,Nullable

### 请求的接口为HttpGet,参数为[FormQuery]或者类似(string userName,int age)格式时

    1，使用GetAdvanced<T>(string url, Dictionary<string, string> request)发起请求，Request为键值对
    返回类型的成员支持Array等类型
    
    2，使用Get<T>(string url, Dictionary<string, string> request)发起请求，Request为键值对
    返回类型的成员只支持Primitives, Decimal,DateTime,String,Guid,List,Dictionary<T1, T2>,Nested classes,Nullable

    3，使用Get(string url, Dictionary<string, string> request)发起请求，Request为键值对
    直接返回string类型的原报文
    
### 请求，返回，异常日志

```json
    {
      "TraceID": "ff2a2ba5-e169-446a-a91b-0dcf020f26c2",
      "MarkType": "PostJsonAdvanced http请求:http://192.168.0.222:48050/ServicePointsForAgent/GetServicePointHasDistribution",
      "ClassName": "RestClientService",
      "MethodName": "PostJsonAdvanced",
      "CreateTime": "2019-07-16 18:22:24.676",
      "LogContent": "请求参数：{\"StippleGuids\":[],\"ProvinceCode\":null} ；返回报文：\"{\\\"Code\\\":0,\\\"Message\\\":null,\\\"Result\\\":[]}\""
    }
```
