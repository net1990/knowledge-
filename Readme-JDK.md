# CentOs安装JDK教程

#### 前提(请注意)：

    1，环境Centos7.6 64位(本示例为虚拟机，IP:192.168.3.235)
    2，jdk1.8(本示例JDK1.8U221)
    
#### 1，下载JDK1.8安装包

    地址：https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
    下载最新的安装包:Java SE Development Kit 8u221，选择Linux x64的rpm包或者tar.gz包
    注：JDK在Oracle官网下载需要用户登录，建议网上搜索一些共享账号登录下载
    
#### 2，解压

    将包在windows上解压出来（本示例：jdk-8u221-linux-x64.tar.gz），然后把解压后的文件夹使用xftp拷贝到usr/java目录下(如果java目录不存在，则新建之，示例得到/usr/java/jdk1.8.0_221)
    您也可以直接在Linux上下载包解压来实现这步骤

#### 3，配置环境变量（在/etc/profile文件中配置）
```c#
    //以编辑模式打开文件
    vi /etc/profile
    
    //在profile文件末尾添加下面三行：注意JAVA_HOME=您的java bin所在的父目录（本例中：/usr/java/jdk1.8.0_221）
    export JAVA_HOME=/usr/java/jdk1.8.0_221
    export PATH=$JAVA_HOME/bin:$PATH
    export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib
```

#### 4，验证：输入java -version，查看版本