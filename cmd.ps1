# 替换后的项目名称
param($newProjectName)
Write-Host "Arg: "$newProjectName;

# 设置输出格式
$OutputEncoding = [Text.UTF8Encoding]::UTF8

## 公共参数

# 替换前的公司名称
$oldCompanyName="KDFF"
# 替换后的公司名称
$newCompanyName="Kdfafa"

# 替换前的项目名称
$oldProjectName="App"
# 替换后的项目名称
#$newProjectName="UserApp"

# 文件类型名称
$fileType="FileInfo"

# 目录类型名称
$dirType="DirectoryInfo"

# sln所在目录
$slnFolder = (Get-Item -Path "./" -Verbose).FullName
#$slnFolder = "D:\Code\Git\Tool.CodeTemplate\Tool.CodeTemplate\bin\Debug\Sources\KDFF"

Write-Host $slnFolder

# 需要修改文件内容的文件后缀名
$include=@("*.cs","*.cshtml","*.asax","*.ts","*.user","*.csproj","*.sln","*.xaml","*.json","*.js","*.xml","*.config","Dockerfile")
#$include=@("*.cs","*.cshtml","*.asax","*.ps1","*.ts","*.csproj","*.user","*.sln","*.xaml","*.json","*.js","*.xml","*.config","Dockerfile")

#$elapsed = [System.Diagnostics.Stopwatch]::StartNew()

# 清理文件夹
#Write-Host "清理文件夹"
#Remove-Item -Recurse (Join-Path $slnFolder ".git")
#Remove-Item -Recurse (Join-Path $slnFolder ".vs")
#Remove-Item -Recurse (Join-Path $slnFolder ".vscode")
#Remove-Item -Recurse (Join-Path $slnFolder "build")
#Remove-Item -Recurse (Join-Path $slnFolder "docker")
#Remove-Item -Recurse (Join-Path $slnFolder "README.md")



#Write-Host "开始重命名文件夹"
# 重命名文件夹
#$_.Name.Contains($oldCompanyName) -or 
Ls $slnFolder -Recurse | Where { $_.GetType().Name -eq $dirType -and ($_.Name.Contains($oldCompanyName) -or $_.Name.Contains($oldProjectName)) } | ForEach-Object{
	Write-Host "directory " $_.FullName;
	$newDirectoryName=$_.Name.Replace($oldCompanyName,$newCompanyName).Replace($oldProjectName,$newProjectName);
	Rename-Item $_.FullName $newDirectoryName;
}
#Write-Host "结束重命名文件夹"
#Write-Host "-------------------------------------------------------------"

# 替换文件中的内容和文件名
#Write-Host "开始替换文件中的内容和文件名"
Ls $slnFolder -Include $include -Recurse | Where { $_.GetType().Name -eq $fileType} | ForEach-Object{
	$fileText = Get-Content $_ -Raw -Encoding UTF8
	if($fileText.Length -gt 0 -and ($fileText.contains($oldCompanyName) -or $fileText.contains($oldProjectName))){
		$fileText.Replace($oldCompanyName,$newCompanyName).Replace($oldProjectName,$newProjectName) | Set-Content $_ -Encoding UTF8 -NoNewline
		Write-Host "file(change text) " $_.FullName
	}
	If($_.Name.contains($oldCompanyName) -or $_.Name.contains($oldProjectName)){
		$newFileName=$_.Name.Replace($oldCompanyName,$newCompanyName).Replace($oldProjectName,$newProjectName)
		Rename-Item $_.FullName $newFileName
		Write-Host "file(change name) " $_.FullName
	}
}
#Write-Host "结束替换文件中的内容和文件名"
#Write-Host "-------------------------------------------------------------"

#$elapsed.stop()
#write-host "共花费时间: $($elapsed.Elapsed.ToString())"




































