## 1.安装dotnet产品提要
#### 要开始安装.NET，您需要注册Microsoft签名密钥并添加Microsoft产品提要。每台机器只需要做一次。 
#### 打开命令提示符并运行以下命令：
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
Sudo sh -c 'echo -e "[packages-microsoft-com-prod]\nname=packages-microsoft-com-prod 
\nbaseurl= https://packages.microsoft.com/yumrepos/microsoft-rhel7.3-prod\nenabled=1
\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > 
/etc/yum.repos.d/dotnetdev.repo'

## 2.安装.NET SDK
#### 更新可用于安装的产品，安装.NET所需的组件，然后安装.NET SDK。
sudo yum update

sudo yum install libunwind libicu

sudo yum install dotnet-sdk-2.2  #最新版本请关注官网

最后检查是否安装成功，输入命令：dotnet --version

如果输出版本号则说明安装成功。

## 3.上传程序
#### 使用Xftp软件上传程序

## 4.发布程序
#### 可使用PuTTY软件远程连接命令操作发布。

安装使用教程参考：https://jingyan.baidu.com/article/90bc8fc8b9bca1f653640ca5.html

发布命令：dotnet web.dll

后台进程启动发布

发布命令：nohup dotnet web.dll & jobs -l

建议使用后台进程启动发布，因为非后台进程启动发布后，dos窗体关闭后发布的应用程序也会一同关闭。

## 5.重新发布
#### (1)使用Xftp软件上传zip格式压缩包
使用yum安装zip解压软件

命令：sudo yum install -y unzip zip

#### (3)查看进程

命令：ps -ef | grep dotnet

#### (4)结束进程

命令：kill -9 进程ID

(5)解压压缩包到指定目录

压缩命令：zip -r /home/kms/kms.zip /home/kms/server/kms

解压命令：unzip publish.zip -d /root/wwwroot/test 

#### (6)发布

发布命令：nohup dotnet web.dll & jobs -l

#### (7)删除压缩包

命令：rm -f /root/wwwroot/test/publish.zip


#### (8)删除指定目录

命令：sudo rm -rf /root/wwwroot/test/ss

说明： -r 向下递归，不管有多少级目录，一并删除

 	   -f 直接强行删除，不作任何提示
 	   
## 6.防火墙开启协议和端口

使用如下方式设置防火墙开启http和https协议并开启5000端口

sudo firewall-cmd --permanent --zone=public --add-service=http 

sudo firewall-cmd --permanent --zone=public --add-service=https

sudo firewall-cmd --permanent --zone=public --add-port=5000/tcp 

sudo firewall-cmd --reload  #重新加载配置

## 7.文件夹创建和文件创建
命令：mkdir /etc/supervisor/conf.d   #创建配置文件夹

命令：touch /etc/supervisor/conf.d/test.conf  #创建配置文件


