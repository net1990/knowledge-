## 1.Logger是什么
Logger是.NET Core自带的一个日志框架，默认实现了“控制台”日志、“调试”日志、“事件”日志、“跟踪”日志。

## 2.如何使用Logger

#### (1)DI中获取ILogger<T>对象创建日志

public class HomeController:Controller

{

    private readonly ILogger _logger;
    
    public HomeController(ILogger<HomeController> logger)
    {
        _logger=logger;
    }
    
    public IActionResult Index()
    {
        _logger.LogInformation(“index日志”);
    }
}

#### (2)程序中创建日志
public static void Main(string[] args) 
{
    var host = CreateWebHostBuilder(args).Build();
    
    var logger = host.Services.GetRequiredService<ILogger<Program>>(); 
    logger.LogInformation("Seeded the database."); 
    host.Run(); 
}

public static IWebHostBuilder CreateWebHostBuilder(string[] args) => 

WebHost.CreateDefaultBuilder(args) .UseStartup<Startup>().

ConfigureLogging(logging => 
{ 
    logging.ClearProviders(); 
    
    logging.AddConsole(); 
});

#### (3)使用ILoggerFactory 显示指定类别创建ILogger
public class TodoController : Controller 
{
    private readonly ILogger _logger; 
    
    public TodoController(ILoggerFactory logger) 
    { 
        _logger=logger.CreateLogger("TodoApiSample.Controllers.TodoController"); 
    }
}

## 3.日志级别
ASP.NET Core 定义了以下日志级别（按严重性从低到高排列）。

(1)跟踪 = 0

有关通常仅用于调试的信息。 这些消息可能包含敏感应用程序数据，因此不得在生产环境中启用它们。 默认情况下禁用。

(2)调试 = 1

有关在开发和调试中可能有用的信息。 示例:Entering method Configure with flag set to true. 由于日志数量过多，因此仅当执行故障排除时，才在生产中启用 Debug 级别日志。

(3)信息 = 2

用于跟踪应用的常规流。 这些日志通常有长期价值。 示例：Request received for path /api/todo

(4)警告 = 3

表示应用流中的异常或意外事件。 可能包括不会中断应用运行但仍需调查的错误或其他条件。 Warning 日志级别常用于已处理的异常。 示例：FileNotFoundException for file quotes.txt.

(5)错误 = 4

表示无法处理的错误和异常。 这些消息指示的是当前活动或操作（例如当前 HTTP 请求）中的失败，而不是整个应用中的失败。日志消息示例：Cannot insert record due to duplicate key violation.

(6)严重 = 5

需要立即关注的失败。 例如数据丢失、磁盘空间不足


## 4.新框架中使用ILogger
#### (1)引用KJFrame.Core.Log.dll

#### (2)在Startup类中ConfigureServices方法中加入如下代码
        services.AddKJLog4net();
#### (3)新建配置文件夹Config,在配置文件夹中新建配置，配置如下：

<log4net>

  <root>
    <level value="DEBUG" />
    <appender-ref ref="DebugRollingFileAppender" />
    <appender-ref ref="InfoRollingFileAppender" />
    <appender-ref ref="WarnRollingFileAppender" />
    <appender-ref ref="ErrorRollingFileAppender" />
    <appender-ref ref="FatalRollingFileAppender" />
  </root>

  <appender name="DebugRollingFileAppender" type="log4net.Appender.RollingFileAppender" >
    <file value="Logs/Debug/" />
    <appendToFile value="true" />
    <immediateFlush value="true" />
    <staticLogFileName value="false" />
    <rollingStyle value="Date" />
    <datePattern value="yyyy\\MM\\yyyy-MM-dd&quot;.log&quot;" />
    <MaximumFileSize value="50MB"/>
    <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss.ffff} [%t] %c :%newline %m%n %newline " />
    </layout>
    <filter type="log4net.Filter.LevelMatchFilter">
      <levelToMatch  value="DEBUG" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>

  <appender name="InfoRollingFileAppender" type="log4net.Appender.RollingFileAppender" >
    <file value="Logs/Info/" />
    <appendToFile value="true" />
    <immediateFlush value="true" />
    <staticLogFileName value="false" />
    <rollingStyle value="Date" />
    <datePattern value="yyyy\\MM\\yyyy-MM-dd&quot;.log&quot;" />
    <MaximumFileSize value="50MB"/>
    <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss.ffff} %p{NDC}:%newline %m%n" />
    </layout>
    <filter type="log4net.Filter.LevelMatchFilter">
      <levelToMatch  value="INFO" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>

  <appender name="WarnRollingFileAppender" type="log4net.Appender.RollingFileAppender" >
    <file value="Logs/Warn/" />
    <appendToFile value="true" />
    <immediateFlush value="true" />
    <staticLogFileName value="false" />
    <rollingStyle value="Date" />
    <datePattern value="yyyy\\MM\\yyyy-MM-dd&quot;.log&quot;" />
    <MaximumFileSize value="50MB"/>
    <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss.ffff} %p{NDC}:%newline %m%n" />
    </layout>
    <filter type="log4net.Filter.LevelMatchFilter">
      <levelToMatch  value="WARN" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>

  <appender name="ErrorRollingFileAppender" type="log4net.Appender.RollingFileAppender" >
    <file value="Logs/Error/" />
    <appendToFile value="true" />
    <immediateFlush value="true" />
    <staticLogFileName value="false" />
    <rollingStyle value="Date" />
    <datePattern value="yyyy\\MM\\yyyy-MM-dd&quot;.log&quot;" />
    <MaximumFileSize value="50MB"/>
    <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss.ffff} %p{NDC}:%newline %m%n" />
    </layout>
    <filter type="log4net.Filter.LevelMatchFilter">
      <levelToMatch  value="ERROR" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>

  <appender name="FatalRollingFileAppender" type="log4net.Appender.RollingFileAppender" >
    <file value="Logs/Fatal/" />
    <appendToFile value="true" />
    <immediateFlush value="true" />
    <staticLogFileName value="false" />
    <rollingStyle value="Date" />
    <datePattern value="yyyy\\MM\\yyyy-MM-dd&quot;.log&quot;" />
    <MaximumFileSize value="50MB"/>
    <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss.ffff} %p{NDC}:%newline %m%n" />
    </layout>
    <filter type="log4net.Filter.LevelMatchFilter">
      <levelToMatch  value="Fatal" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>

</log4net>

        
#### (4)在控制器构造函数中接收注入对象
        private readonly ILogger _logger;
        public 类名(ILogger logger)
        {
            this._logger = logger;
        }

#### (5) 记录日志
        _logger.LogKJInformation(1, "首页日志", "首页日志信息", this.GetType().FullName, "Index");
        





