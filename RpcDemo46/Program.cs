﻿using ApiService;
using ApiService.Request;
using KJFrame.Core.Rpc.Client46;
using KJFrame.Core.Rpc.Protocol46.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RpcDemo46
{
    class Program
    {
        async static Task Main(string[] args)
        {
            var rpcClientOptions = new RpcClientOptions()
            {
                Host = "127.0.0.1",
                Port = 12003,
                EnableServiceDiscovery = false
            };
            var rpcService = new DefaultRpcServiceDiscovery(rpcClientOptions);
            var serialize = new ProtoBufSerializer();
            var userService = RpcClientFactory.Create<IUserService>(
                new DefaultRpcChannel(rpcService, serialize, rpcClientOptions));
            var request = new IdRequest
            {
                Id = 1000
            };

            while (true)
            {
                try
                {
                    Console.Write("Please input keyword:");
                    var input = Console.ReadLine();
                    if (input.Equals("Q", StringComparison.OrdinalIgnoreCase))
                    {
                        break;
                    }


                    var tokenSource = new CancellationTokenSource(1000 * 10);

                    var userDto = await userService.GetBill(request, tokenSource.Token);

                    Console.WriteLine(userDto.Message);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }

            Console.WriteLine("press any key to exit.");
            Console.ReadKey(true);
        }
    }
}
