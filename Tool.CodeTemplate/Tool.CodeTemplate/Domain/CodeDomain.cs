﻿using System;
using System.IO;
using Tool.CodeTemplate.Help;
using System.Collections.ObjectModel;
using System.Diagnostics;
using RestSharp;
using RestSharp.Extensions;
using System.Text;

namespace Tool.CodeTemplate.Domain
{
    public class CodeDomain
    {
        /// <summary>
        /// 配置
        /// </summary>
        public Setting Setting { get; set; }

        public CodeDomain() { }

        public CodeDomain(Setting setting)
        {
            Setting = setting;
        }

        public void Run()
        {
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), $@"Sources\{Setting.SourceName}");
            //if (!File.Exists(filePath))
            //{
            //    throw new Exception($"文件不存在:{filePath}");
            //}

            string cmdPath = Path.Combine(Directory.GetCurrentDirectory(), $@"Sources\{Setting.CmdName}");
            if (!File.Exists(cmdPath))
            {
                throw new Exception($"命令脚本不存在:{filePath}");
            }

            string toPath = Path.Combine(Directory.GetCurrentDirectory(), "Sources");
            string exitPath = toPath + "\\" + Setting.Source;
            if (Directory.Exists(exitPath))
            {
                Console.WriteLine("删除旧的解压文件..........");
                Directory.Delete(exitPath, true);
                Console.WriteLine("删除旧的解压文件成功");
            }

            if (File.Exists(filePath))
            {
                Console.WriteLine("删除旧的源码..........");
                File.Delete(exitPath);
                Console.WriteLine("删除旧的源码成功");
            }

            Console.WriteLine("下载源码..........");
            Download(filePath);
            Console.WriteLine("下载源码成功");

            Console.WriteLine("解压源码..........");
            UnPack(filePath, toPath);
            Console.WriteLine("解压源码成功");

            Console.WriteLine("执行命令脚本..........");
            RunCmd(cmdPath); 
            //System.Threading.Thread.Sleep(2000);
            //Process.Start(exitPath);
            //RunChangeFile(exitPath);
            Console.WriteLine("执行命令脚本成功");
        }

        private void Download(string path)
        {
            var client = new RestClient("https://gitlab.com/net1990/knowledge-/raw/master/Tool.CodeTemplate/Tool.CodeTemplate/Sources/KdCode.zip");
            IRestRequest request = new RestRequest();
            client.DownloadData(request).SaveAs(path);
        }

        public bool UnPack(string filePath, string toPath)
        {
            return ZipHelper.UnZip(filePath, toPath);
        }

        public void RunChangeFile(string exitPath)
        {
            DirectoryInfo directoryinfo = new DirectoryInfo(exitPath);
            var items = directoryinfo.GetFiles();
            if (items != null)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] is FileInfo)
                    {
                        var temp = items[i] as FileInfo;
                        if (Setting.FileTypes.Contains(temp.Extension.ToLower()))
                        {
                            var files = File.ReadAllText(temp.FullName, Encoding.Default);
                            if (temp.Name == "HomeController.cs")
                            {
                                System.Console.WriteLine(" ");
                            }
                            if (!string.IsNullOrEmpty(files))
                            {
                                var newFiles = files.Replace(Setting.OldCompanyName, Setting.CompanyName).Replace(Setting.OldProjectName, Setting.ProjectName).Trim();
                                File.WriteAllText(temp.FullName, newFiles, Encoding.Default);
                            }
                        }
                    }
                }
            }
            var folders = Directory.GetDirectories(exitPath);
            if (folders != null) {
                foreach (var folder in folders) {
                    RunChangeFile(folder);
                }
            }
        }

        public void RunCmd(string path)
        {
            #region 
            //RunCmd(@"D:\Code\Git\Tool.CodeTemplate\Tool.CodeTemplate\Sources\cmd.ps1", Setting.ProjectName); 
            #endregion
            //var process = Process.Start("pwsh.exe", "&" + path + " " + Setting.ProjectName);
            Process.Start("Powershell.exe", "&" + path + " " + Setting.ProjectName);
        }
    }
}
