﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tool.CodeTemplate.Domain;
using Tool.CodeTemplate.Enums;

namespace Tool.CodeTemplate
{
    class Program
    {
        static void Main(string[] args)
        {
            Setting setting = new Setting()
            {
                SourceName = ConstConfig.SourceName,
                Source = ConstConfig.Source,
                CmdName = ConstConfig.CmdName
            };
            Console.WriteLine("启动..........");
            Console.WriteLine("请输入项目名（示例:Pet；输出的项目：Kdfafa.Pet.Api...........）");
            string temp = string.Empty;
            temp = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(temp))
            {
                Console.WriteLine("请输入正确的项目名..........");
                temp = Console.ReadLine();
            }
            setting.ProjectName = temp.Trim();
            CodeDomain domain = new CodeDomain(setting);
            try
            {
                domain.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine("..........................................................................................");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            Console.ReadKey();
        }
    }

    public class Setting
    {
        public string SourceName { get; set; }
        public string Source { get; set; }
        public string CmdName { get; set; }
        public string ProjectName { get; set; }

        public string OldProjectName
        {
            get { return "App"; }
        }

        public string OldCompanyName
        {
            get { return "KDFF"; }
        }

        public string CompanyName
        {
            get { return "Kdfafa"; }
        }

        public List<string> FileTypes
        {
            get
            {
                return new List<string>{ ".cs", ".cshtml", ".asax", ".user", ".ts", ".csproj", ".sln", ".xaml", ".json", ".js", ".xml", ".config" };
            }
        }

    }
}
