# 1,简介

    1，日志有两个组件：KJFrame.Core.Log;KJFrame.Core.TraceLog
    2，KJFrame.Core.TraceLog依赖于KJFrame.Core.Log
    3，KJFrame.Core.Log用于写特定Json格式日志，基于Log4Net封装的组件；按照快金的日志收集系统的Json格式输出日志
    4，KJFrame.Core.TraceLog用于输出具有跟踪TraceId的日志；如在记录每一个Http请求在Controller，Service中的日志时，使用该组件写日志可以保证TraceID相同
    5，无论是KJFrame.Core.Log,还是KJFrame.Core.TraceLog，我们推荐使用Info方法输出日志(便于ES日志收集)，而不推荐Debug,Error等其他方法
    6，如果您不需要使用具有跟踪TraceId的日志，只需引入KJFrame.Core.Log，可以不引用KJFrame.Core.TraceLog组件

## 配置

    1，因为底层是调用Log4Net进行日志输出，所以需要在项目中配置Config/log4net.config文件
    2，注意Config首字母大写，，在部署Linux下时，文件系统是大小写敏感的
    3，log4net.config中的File参数的值注意字母的大小写
   
   ```c#
  <log4net>
      <root>
        <level value="INFO" />
        <appender-ref ref="ErrorRollingLogFileAppender" />
      </root>
      <appender name="ErrorRollingLogFileAppender" type="log4net.Appender.RollingFileAppender,log4net" LEVEL="ERROR">
        <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
        
        <!--Linux使用此路径便于日志收集-->
        <!--<param name="File" value="/data/logs/Kdfafa.Agent.Api/info.log" />-->
        
        <!--Windows使用此路径便于开发调试-->
        <param name="File" value="Logs/info.log" />
        
        <param name="AppendToFile" value="true" />
        <param name="RollingStyle" value="Size" />
        <param name="StaticLogFileName" value="true" />
        <param name="MaxSizeRollBackups" value="-1" />
        <param name="MaximumFileSize" value="5MB" />
        <layout type="log4net.Layout.PatternLayout,log4net">
          <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss.ffff} %p{NDC}:%newline %m%n" />
        </layout>
        <filter type="log4net.Filter.LevelRangeFilter">
          <param name="LevelMin" value="INFO" />
          <param name="LevelMax" value="FATAL" />
        </filter>
      </appender>
    </log4net>
   ```
   
## 常规日志KJFrame.Core.Log

### 1，引入包
    KJFrame.Core.Log
    
### 2，Startup.cs的ConfigureServices()方法中注册服务
   ```c#
    services.AddKJLog4net();
   ```
### 3，写日志
   ```c#
    public class LogTestController : Controller
    {

        /// <summary>
        /// 日志
        /// </summary>
        private readonly IKJLogger _logger;

        public LogTestController(IKJLogger logger)
        {
            _logger = logger;
        }


        public string Index(string userName)
        {
            //_logger.Info("MarkType", "内容", "类名", "方法名");
            _logger.Info("Api Index", "测试日志", "HomeController", "Index");
            
            //_logger.Info("MarkType","内容");，此方法自解析类名，方法名
            _logger.Info("Api Index","输出测试内容");
            
            return "test";
        }
    }
   ```

# 跟踪日志KJFrame.Core.TraceLog

    TraceID原理:
    1，TraceLog的TraceID是依赖于一个KJFrame.Core.ITrace的组件中的ITrace接口获得的，ITrace中包含一个TraceID,通过将ITrace接口注册成IScopedService,
    使得在每一次IScopedService请求生命周期中，获得的ITrace的实例都是相同的，所以TraceID也是相同的；
    2，只有在IScopedService父生命周期中的ITraceLogger才能实现TraceID在同一个请求/线程中相同，
    如果父生命周期是ISingleSercice(单例)，那么所有请求中的TraceID都是一样的
    如果父生命周期是ITransientService的，那么整个生命周期中的TraceID都是不一样的
    3，一般情况下，我们都是在IScopedService的生命周期中调用TraceLog,即在IScopedService接口的实现类中调用TraceLog


### 1，引入包
    KJFrame.Core.TraceLog
    
### 2，Startup.cs的ConfigureServices()方法中注册服务
   ```c#
   //注册KJFrame.Core.Log服务，因为TrcaeLog依赖Log
    services.AddKJLog4net();
    
   //注册具有TrcaeID的Log
    services.AddTraceLog();
    
   ```
### 3，写日志
   ```c#
    public class LogTestController : Controller
    {
        //注入ITraceLogger
        private readonly ITraceLogger _logger;

        public LogTestController(ITraceLogger logger)
        {
            _logger = logger;
        }


        public string Index(string userName)
        {
            //_logger.Info("MarkType","内容");，此方法自解析类名，方法名,TraceId
            _logger.Info("Api Index","输出测试内容");
            
            return "test";
        }
    }
   ```

### 4，跟踪日志样例（每一层的TraceID都是一致的）

// Controller层
```json
{
  "TraceID": "0dd95d11-8cab-41b4-84c9-40110c171480",
  "MarkType": "Api请求 GetAgentInfoByPage",
  "ClassName": "AgentController",
  "MethodName": "<GetAgentInfoByPage>b__0",
  "CreateTime": "2019-07-16 18:21:00.504",
  "LogContent": "Controller层请求参数:{\"ProvinceCode\":\"\",\"CityCode\":\"\",\"AreaCode\":\"\",\"KeyWord\":\"\",\"PageIndex\":1,\"PageSize\":5}"
}
```

```json
// Controller层
{
  "TraceID": "0dd95d11-8cab-41b4-84c9-40110c171480",
  "MarkType": "Api请求 GetAgentInfoByPage",
  "ClassName": "AgentController",
  "MethodName": "<GetAgentInfoByPage>b__0",
  "CreateTime": "2019-07-16 18:21:00.504",
  "LogContent": "Controller层请求参数:{\"ProvinceCode\":\"\",\"CityCode\":\"\",\"AreaCode\":\"\",\"KeyWord\":\"\",\"PageIndex\":1,\"PageSize\":5}"
}
```

```json
//服务层
{
  "TraceID": "0dd95d11-8cab-41b4-84c9-40110c171480",
  "MarkType": "数据访问 AgentServicePointService GetAgentInfoByPage",
  "ClassName": "AgentServicePointService",
  "MethodName": "GetAgentInfoByPage",
  "CreateTime": "2019-07-16 18:21:00.510",
  "LogContent": "服务层请求参数:{\"ProvinceCode\":\"\",\"CityCode\":\"\",\"AreaCode\":\"\",\"KeyWord\":\"\",\"PageIndex\":1,\"PageSize\":5}"
}
```

```json
//服务层调用Restsharp的HttpClientService写的日志(此处摘抄)
{
  "TraceID": "0dd95d11-8cab-41b4-84c9-40110c171480",
  "MarkType": "GetAdvanced http请求:http://192.168.0.222:48050/ServicePointsForAgent/GetServicePointInfos",
  "ClassName": "RestClientService",
  "MethodName": "GetAdvanced",
  "CreateTime": "2019-07-16 18:21:00.747",
  "LogContent": "请求参数：null ；返回报文：\"{\\\"Code\\\":0,\\\"DebugMsg\\\":null,\\\"Message\\\":null,\\\"Result\\\":[]}\""
}
```