ElasticSearch安装教程

#### 前提(请注意)：

    1，环境Centos7.6 64位(本示例为虚拟机，IP:192.168.3.235)
    2，已安装jdk1.8(本示例JDK1.8U221) 
    3，此教程针对ElasticSearch6.8.1的安装（2019-07-16）

#### 下载

    1，下载最新版本（不推荐）：https://www.elastic.co/downloads/elasticsearch
    2，下载指定版本（本示例版本6.8.1）:https://www.elastic.co/downloads/past-releases#elasticsearch
    3，注意下载时选择Linux版本
    4，关于版本，由于最新版本往往依赖的JDK版本也较高（如JDK10），所以您可能需要根据各个版本的依赖环境来选择，您可以在
    https://www.elastic.co/guide/en/elasticsearch/reference/index.html看到ElasticSearch的安装依赖关系

#### 解压
    1，在CentOS7的home目录下新建elasticsearch文件夹（/home/elasticsearch）
    2，在windows上将下载下来的压缩包解压出来，然后将解压后的文件夹中的文件使用xftp拷贝到CentOS的/home/elasticsearch下
        或者您也可以直接在CentOS上执行解压操作
    3，无论您采用何种方式解压或者下载；解压后的文件放在了/home/elasticsearch下【说明得到的结果：/home/elasticsearch/bin】

#### 配置
######    A，修改elasticsearch.yml配置（本例 /home/elasticsearch/config/elasticsearch.yml ）
```c#
    //以编辑模式打开elasticsearch.yml配置文件
    vi /home/elasticsearch/config/elasticsearch.yml
```
    按如下修改，并保存
```c#
    #此处省略了其他代码
    # ---------------------------------- Cluster -----------------------------------
    # Use a descriptive name for your cluster:
    cluster.name: Agent #修改集群的名称(示例Agent)
    #此处省略了其他代码
    # ------------------------------------ Node ------------------------------------
    # Use a descriptive name for the node:
    node.name: node-1 #修改本机器在集群中的节点名称(示例node-1)
    #此处省略了其他代码
    # ---------------------------------- Network -----------------------------------
    # Set the bind address to a specific IP (IPv4 or IPv6):
    network.host: 0.0.0.0 #修改IP地址，用于外网访问(注如果配127.0.0.1可能无法外网访问)
    #此处省略了其他代码
    http.port: 9200 #修改端口，用于外网访问(注默认就是9200)
    #此处省略了其他代码
```

######    B，修改limits.conf配置(/etc/security/limits.conf)
    
    这里的修改用于解决这个错误：max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536]
    
```c#
    //以编辑模式打开limits.conf配置文件
    vi /etc/security/limits.conf
```

    按如下修改，并保存
```c#
    #直接在文件末尾添加如下内容，注意es-admin是用于启动elasticsearch的用户，我们下面会告诉我们如何创建这个用户
    es-admin hard nofile 65536 #这个井号后面的是备注，备注可以不添加进去，es-admin是用于启动elasticsearch的用户，root是不允许启动elasticsearch的
    es-admin soft nofile 65536 #这个井号后面的是备注，备注可以不添加进去，es-admin是用于启动elasticsearch的用户，root是不允许启动elasticsearch的
    es-admin soft memlock unlimited #这个井号后面的是备注，备注可以不添加进去，es-admin是用于启动elasticsearch的用户，root是不允许启动elasticsearch的
    es-admin hard memlock unlimited #这个井号后面的是备注，备注可以不添加进去，es-admin是用于启动elasticsearch的用户，root是不允许启动elasticsearch的
```
######    C，修改配置

    这里的修改用于解决这个错误：max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

    执行以下命令
        sysctl -w vm.max_map_count=262144
    再执行以下命令
        more /proc/sys/vm/max_map_count


####  创建用于启动elasticsearch的用户（本示例：es-admin)
    
    1，elasticsearch是不允许使用root用户启动的，所以需要新建一个用户
```c#    
    //新建用户，这里的es-admin即是上文配置中所提到的
    useradd es-admin
    //设置用户密码(密码要输入两次：大小写，数字，特殊字符)
    password es-admin
    //目录授权,先进入/home/elasticsearch目录，再chown -R es-admin *
    cd /home/elasticsearch
    chown -R es-admin *
    
    //如果启动elasticsearch过程中报elasticsearch目录下某个文件没有权限执行，或者访问，可以使用下面命令解决
    sudo chmod -R 777 /home/elasticsearch(授予文件的读写执行操作，777具有特殊的含义，不是瞎写的，也不要瞎换)
```

#### 配置端口
    
    //打开所需要用到的端口，比如：9200
    firewall-cmd --add-port=9200/tcp --permanent

#### 启动elasticsearch
```c#    
    //cd到elasticsearch的bin目录下
    cd  /home/elasticsearch/bin
    
    //切换成es-admin用户
    su  es-admin
    
    //启动
    ./elasticsearch
   
```

#### 访问验证，在浏览器中打开（你的虚拟机的ip地址）：http://192.168.3.235:9200/

    
    
