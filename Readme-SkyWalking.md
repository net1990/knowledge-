# ```SkyWalking服务跟踪```

    参考文献
    https://www.cnblogs.com/shook/p/10852691.html
    https://www.cnblogs.com/landonzeng/p/10616644.html
    
#### 前提(请注意)：

    1，环境Centos7.6 64位(本示例为虚拟机，IP:192.168.3.235)
    2，已安装jdk1.8(本示例JDK1.8U221) 
    3，已安装ElasticSearch6.8.1
    
[**参考JDK安装教程**](https://gitlab.com/net1990/knowledge-/blob/master/Readme-JDK.md)

[**参考ElasticSearch安装教程**](https://gitlab.com/net1990/knowledge-/blob/master/Readme-ElasticSearch.md)
     
&emsp;
&emsp;
&emsp;
&emsp;
&emsp;
&emsp;
# ```CentOS安装SkyWalking服务端教程（SkyWalking6.2）```

#### 1，下载skywalking

    前往http://skywalking.apache.org/downloads/选择Linux二进制版本下载
    本示例的下载地址http://mirror.bit.edu.cn/apache/skywalking/6.2.0/apache-skywalking-apm-6.2.0.tar.gz
    
#### 2，解压

    将包在windows上解压出来，然后把解压后的文件夹使用xftp拷贝到home目录下(示例得到/home/skywalking6.2)
    您也可以直接在Linux上下载包解压来实现这步骤

#### 3，修改/home/skywalking6.2/config/application.yml

```c#
    core:
      default:
        # 此处省略了其他代码
        role: ${SW_CORE_ROLE:Mixed} # Mixed/Receiver/Aggregator
        restHost: ${SW_CORE_REST_HOST:192.168.3.235}#服务器名称
        restPort: ${SW_CORE_REST_PORT:12800}
        restContextPath: ${SW_CORE_REST_CONTEXT_PATH:/}
        gRPCHost: ${SW_CORE_GRPC_HOST:192.168.3.235}#服务器名称
        gRPCPort: ${SW_CORE_GRPC_PORT:11800}
    	
    	# 此处省略了其他代码
    storage:
      elasticsearch:
        nameSpace: ${SW_NAMESPACE:"Agent"}#不知道是什么意思，这里我填的是es的集群的名称
        clusterNodes: ${SW_STORAGE_ES_CLUSTER_NODES:192.168.3.235:9200}#es服务器
        user: ${SW_ES_USER:"es-admin"}#es用户
        password: ${SW_ES_PASSWORD:"Nirvana!95"} #es用户密码
        indexShardsNumber: ${SW_STORAGE_ES_INDEX_SHARDS_NUMBER:2}
        indexReplicasNumber: ${SW_STORAGE_ES_INDEX_REPLICAS_NUMBER:0}
        # 此处省略了其他代码
        bulkActions: ${SW_STORAGE_ES_BULK_ACTIONS:2000} 
        bulkSize: ${SW_STORAGE_ES_BULK_SIZE:20} 
        flushInterval: ${SW_STORAGE_ES_FLUSH_INTERVAL:10} 
        concurrentRequests: ${SW_STORAGE_ES_CONCURRENT_REQUESTS:2} 
        # 此处省略了其他代码
```

#### 4，修改/home/skywalking6.2/webapp/webapp.yml

```c#
    server:
      port: 8080
      
    collector:
      path: /graphql
      ribbon:
        ReadTimeout: 10000
        # Point to all backend's restHost:restPort, split by ,
        listOfServers: 192.168.3.235:12800 #服务器名称
```    

#### 5，打开端口(如果您已打开11800,12800，8080等相应端口，请忽略此步骤)

    打开端口使用此命令，本处仅以8080端口示例
    firewall-cmd  --add-port=8080/tcp --permanent

#### 6，执行/home/skywalking6.2/bin/startup.sh

```c#
    cd /home/skywalking6.2
    ./bin/startup.sh
```

#### 7，打开Skywalking监控的UI界面

    http://192.168.3.235:8080
    
&emsp;
&emsp;
&emsp;
&emsp;
&emsp;
&emsp;
# ```.NET Core项目集成SkyWalking教程```

#### 1，在NuGet引入程序集SkyAPM.Agent.AspNetCore

```c#
    SkyAPM.Agent.AspNetCore
```
#### 2，在项目中配置环境变量

    配置环境变量有两种方式，这里只讲在单个项目中的配置
    
    在项目的Properties/launchSettings.json做如下修改
```json    
    {
      //此处省略了其他代码
      "profiles": {
        "IIS Express": {
          "commandName": "IISExpress",
          "launchBrowser": true,
          "launchUrl": "api/values",
          "environmentVariables": {
            "ASPNETCORE_ENVIRONMENT": "Development",
            
            //添加
            "ASPNETCORE_HOSTINGSTARTUPASSEMBLIES": "SkyAPM.Agent.AspNetCore",
            
            //添加,Skywalking_App为项目名称(此处规则没有研究)，本例的项目名称为Skywalking.App
            "SKYWALKING__SERVICENAME": "Skywalking_App"
          }
        },
        "Skywalking.App": {
          "commandName": "Project",
          "launchBrowser": true,
          "launchUrl": "api/values",
          "applicationUrl": "http://localhost:5000",
          "environmentVariables": {
            "ASPNETCORE_ENVIRONMENT": "Development",
            
            //添加
            "ASPNETCORE_HOSTINGSTARTUPASSEMBLIES": "SkyAPM.Agent.AspNetCore",
            
            //添加,Skywalking_App为项目名称(此处规则没有研究)，本例的项目名称为Skywalking.App
            "SKYWALKING__SERVICENAME": "Skywalking_App"
          }
        }
      }
    }
```
#### 配置客户端的SkyWalking

    1，以管理员启动cmd(注意以管理员省份启动，这是个坑，如果不使用管理员执行下面命令，不仅可能会失败，还有可能会无法重装[重新执行命令])
    
    2，cd项目目录下，如本例的：D:\Code\Git4\Skywalking.App\Skywalking.App
    
    3，安装SkyAPM.DotNet.CLI工具
    
```c#
        dotnet tool install -g SkyAPM.DotNet.CLI
```
    
    4，生成skyapm配置文件：skyapm.json（本示例中该文件会生成在D:\Code\Git4\Skywalking.App\Skywalking.App目录下）
    
    说明：Skywalking_App为项目在launchSettings.json配置的名称，192.168.3.235:11800为之前安装在Centos7上的SkyWalking的服务端的Grpc地址
    
```c#
    dotnet skyapm config Skywalking_App 192.168.0.1:11800
```
    5，生成结果的skyapm.json（说明，您也可以不使用SkyAPM.DotNet.CLI，而采用直接按照SkyWalking的配置规范写一个skyapm.json）
```c#    
    {
      "SkyWalking": {
        "ServiceName": "Skywalking_App",
        "Namespace": "",
        "HeaderVersions": [
          "sw6"
        ],
        "Sampling": {
          "SamplePer3Secs": -1,
          "Percentage": -1.0
        },
        "Logging": {
          "Level": "Information",
          "FilePath": "logs\\skyapm-{Date}.log"
        },
        "Transport": {
          "Interval": 3000,
          "ProtocolVersion": "v6",
          "QueueSize": 30000,
          "BatchSize": 3000,
          "gRPC": {
            "Servers": "192.168.3.235:11800",
            "Timeout": 10000,
            "ConnectTimeout": 10000,
            "ReportTimeout": 600000
          }
        }
      }
    }
```

#### 调试您的.Net Core项目（注意要把您的ElasticSearch,SkyWalking服务器开启，并确保能访问到【可能会被防火墙挡住】）

    启动调试后，将会在项目logs文件夹下看到SkyWalking的日志