﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Model
{
    public class AppDTO
    {
        public string UserName { get; set; }

        public int? Age { get; set; }
    }
}
