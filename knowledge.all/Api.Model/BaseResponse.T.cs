﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Model
{
    public class BaseResponse<T>
    {
        /// <summary>
        /// 状态（成功Succeed=0 , ErrorEx=1 ,  WarnEx = 2)
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 系统逻辑异常信息
        /// </summary>
        public string DebugMsg { get; set; }

        /// <summary>
        /// 警告信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        public T Result { get; set; }

        public BaseResponse()
        {

        }

        public static BaseResponse<T> Ok(string message)
        {
            return new BaseResponse<T>() { Code = 0, Message = message };
        }

        public static BaseResponse<T> Success(T data)
        {
            return new BaseResponse<T>() { Code = 0, Result = data };
        }

        public static BaseResponse<T> Success(string message, T data)
        {
            return new BaseResponse<T>() { Code = 0, Message = message, Result = data };
        }

        public static BaseResponse<T> No(string message)
        {
            return new BaseResponse<T>() { Code = 1, Message = message };
        }

        public static BaseResponse<T> Fail(T data)
        {
            return new BaseResponse<T>() { Code = 1, Result = data };
        }

        public static BaseResponse<T> Fail(string message, T data)
        {
            return new BaseResponse<T>() { Code = 1, Message = message, Result = data };
        }

        public static BaseResponse<T> Get(int code, string message)
        {
            return new BaseResponse<T>() { Code = code, Message = message };
        }

        public static BaseResponse<T> Get(int code, string message, T data)
        {
            return new BaseResponse<T>() { Code = code, Message = message, Result = data };
        }

    }
}
