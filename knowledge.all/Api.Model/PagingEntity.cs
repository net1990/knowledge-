﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Api.Model
{
    /// <summary>
    /// 分页实体
    /// </summary>
    public class PagingEntity:Page
    {
        /// <summary>
        /// 是否分页
        /// </summary>
        [DefaultValue(false)]
        public bool AllowPaging { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        [DefaultValue(0)]
        public int TotalCount { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        [DefaultValue(0)]
        public int TotalPage { get; set; }

        /// <summary>
        /// 排序条件，用于前台查询传入排序字段，格式："字段1 DESC,字段2"
        /// </summary>
        public string OrderBy { get; set; }
    }
}
