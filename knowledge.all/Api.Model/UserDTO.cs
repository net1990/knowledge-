﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Model
{
    [Serializable]
    public class UserDTO
    {

        public UserDTO()
        { }

        public List<string> UserIds { get; set; }

        public List<int> AppIDs { get; set; }

        public int Amount { get; set; }

        public decimal? Quantity { get; set; }

        public string UserName { get; set; }
    }
}
