﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Api.Model
{
    /// <summary>
    /// 页码请求
    /// </summary>
    public class Page
    {
        /// <summary>
        /// 第几页
        /// </summary>
        [DefaultValue(1)]
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页数量
        /// </summary>
        [DefaultValue(15)]
        public int PageSize { get; set; }
    }
}
