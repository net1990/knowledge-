﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Model
{
    /// <summary>
    /// 分页
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagingEntity<T>:PagingEntity
    {
        /// <summary>
        /// 行
        /// </summary>
        public IEnumerable<T> Rows { get; set; }
    }
}
