﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestSharpClient
{
    /// <summary>
    /// 
    /// </summary>
    public class GetServicePointDetailsParam
    {
        /// <summary>
        /// 网点guid
        /// </summary>
        public Guid[] StippleGuids { set; get; }

        /// <summary>
        /// 省份编码
        /// </summary>
        public string ProvinceCode { set; get; }

        /// <summary>
        /// 城市编码
        /// </summary>
        public string CityCode { set; get; }

        /// <summary>
        /// 区域编码
        /// </summary>
        public string AreaCode { set; get; }

        /// <summary>
        ///  0 未认领  1 已认领    2 已认证   3 已关闭  
        /// </summary>
        public int? PointState { set; get; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWord { set; get; }

        /// <summary>
        /// 页码
        /// </summary>
        public int? PageIndex { set; get; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public int? PageSize { set; get; }
    }
}
