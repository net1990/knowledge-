﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestSharpClient
{
    public class ServicePointDetailResultForAgent
    {
        /// <summary>
        /// 网点Guid
        /// </summary>
        public Guid StippleGuid { set; get; }

        /// <summary>
        /// 网点编码
        /// </summary>
        public string StippleCode { set; get; }

        /// <summary>
        /// 网点名称
        /// </summary>
        public string PointName { set; get; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Linkman { set; get; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile { set; get; }

        /// <summary>
        /// 省份编码
        /// </summary>
        public string ProvinceCode { set; get; }

        /// <summary>
        /// 省份名称
        /// </summary>
        public string ProvinceName { set; get; }

        /// <summary>
        /// 城市编码
        /// </summary>
        public string CityCode { set; get; }

        /// <summary>
        /// 城市名称
        /// </summary>
        public string CityName { set; get; }

        /// <summary>
        /// 区域编码
        /// </summary>
        public string AreaCode { set; get; }

        /// <summary>
        /// 区域名称
        /// </summary>
        public string AreaName { set; get; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string Address { set; get; }

        /// <summary>
        /// 经度
        /// </summary>
        public decimal Longitude { set; get; }

        /// <summary>
        /// 纬度
        /// </summary>
        public decimal Latitude { set; get; }

        /// <summary>
        /// 物流公司编码
        /// </summary>
        public string ExpCode { set; get; }

        /// <summary>
        /// 物流公司名称
        /// </summary>
        public string ExpCompany { set; get; }
    }
}
