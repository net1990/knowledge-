﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Model;
using KJFrame.Core.HttpHelp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace RestSharpClient.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ILogger _logger;

        /// <summary>
        ///构造函数
        /// </summary>
        public HomeController(ILogger<RestDemoController> logger)
        {
            this._logger = logger;
            _logger.LogInformation("初始化HomeController完成");
        }

        /// <summary>
        /// Post [FromBody]AppDTO dto
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> Index()
        {
            _logger.LogInformation("已接收到您的请求，Index");
            //_logger.LogDebug("Debug信息输出");
            AppDTO param = new AppDTO()
            {
                UserName = "Post body [body]请求",
                Age = 1
            };
            return await Task.FromResult(param);
        }
    }
}