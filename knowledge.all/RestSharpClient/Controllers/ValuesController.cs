﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace RestSharpClient.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public string Index()
        {
            var client = new RestClient("http://192.168.0.222:48050/ServicePointsForAgent/GetServicePointHasDistribution");
            //var client = new RestClient("http://localhost:56897/ServicePointsForAgent/GetServicePointHasDistribution");
            var request = new RestRequest(Method.POST);
            //request.AddHeader("Content-Type", "application/json");
            //request.AddHeader("Accept", "application/json");
            #region 全填
            //request.AddParameter("StippleGuids", new string[] { "A82BF81D-BFF4-43FA-9911-00003572D46A" });
            var a = new { StippleGuids = new string[] { "A82BF81D-BFF4-43FA-9911-00003572D46A" } };
            request.AddJsonBody(JsonConvert.SerializeObject(a));
            request.AddParameter("ProvinceCode", "");
            request.AddParameter("CityCode", "");
            request.AddParameter("AreaCode", "");
            request.AddParameter("PointState", 1);
            request.AddParameter("KeyWord", "");
            request.AddParameter("PageIndex", 1);
            request.AddParameter("PageSize", 10);
            #endregion
            var response = client.Execute(request);
            var content = response.Content;
            return content;
        }


        public string Index2()
        {
            var client = new RestClient("http://192.168.0.222:48050/ServicePointsForAgent/GetServicePointHasDistribution");
            var request = new RestRequest(Method.POST);
            GetServicePointDetailsParam param = new GetServicePointDetailsParam()
            {
                StippleGuids = new Guid[] {new Guid("A82BF81D-BFF4-43FA-9911-00003572D46A") },
                PageIndex = 1,
                PageSize = 10
            };
            request.AddJsonBody(param);
            var response = client.Execute(request);
            var content = response.Content;
            return content;
        }

        public string IndexGet()
        {
            var client = new RestClient("http://192.168.0.222:48050/ServicePointsForAgent/GetServicePointHasDistribution");
            var request = new RestRequest(Method.POST);
            GetServicePointDetailsParam param = new GetServicePointDetailsParam()
            {
                StippleGuids = new Guid[] { new Guid("A82BF81D-BFF4-43FA-9911-00003572D46A") },
                PageIndex = 1,
                PageSize = 10
            };
            request.AddJsonBody(param);
            var response = client.Post(request);
            var content = response.Content;
            return content;
        }

    }
}
