﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Model;
using KJFrame.Core.HttpHelp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace RestSharpClient.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RestDemoController : ControllerBase
    {
        private readonly IRestClientService _restClientService;

        private readonly ILogger _logger;

        /// <summary>
        ///构造函数
        /// </summary>
        public RestDemoController(IRestClientService restClientService,ILogger<RestDemoController> logger)
        {
            _restClientService = restClientService;
            this._logger = logger;
        }

        /// <summary>
        /// Post [FromBody]AppDTO dto
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp()
        {
            //_logger.LogInformation("测试日志输出");
            _logger.LogDebug("Debug信息输出");
            var client = new RestClient("http://localhost:5009/api/RestApi/PostAppBody");
            var request = new RestRequest(Method.POST);
            AppDTO param = new AppDTO()
            {
                UserName = "Post body [body]请求",
                Age = 1
            };
            request.AddJsonBody(param);
            //var response = client.Post<AppDTO>(request);
            var response = client.Post(request);
            var data = JsonConvert.DeserializeObject<AppDTO>(response.Content);
            return await Task.FromResult(data);
        }

        /// <summary>
        /// Post2 [FromBody]AppDTO dto
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp2()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/PostAppBody");
            var request = new RestRequest(Method.POST);
            AppDTO param = new AppDTO()
            {
                UserName = "Post body 2 [body]请求",
                Age = 1
            };
            request.AddJsonBody(param);
            var response = client.Post<AppDTO>(request);
            return await Task.FromResult(response.Data);
        }

        /// <summary>
        /// Post ([FromBody]AppDTO dto)
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp3()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/PostAppBody");
            var request = new RestRequest();
            AppDTO param = new AppDTO()
            {
                UserName = "Post body 3请求",
                Age = 1
            };
            request.AddJsonBody(param);
            var response = client.Post<AppDTO>(request);
            return await Task.FromResult(response.Data);
        }

        /// <summary>
        /// PostApp4 [FromQuery]
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp4()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/PostAppUrl");
            var request = new RestRequest();
            request.AddQueryParameter("UserName", "高级传参");
            request.AddQueryParameter("Age", "12");
            // from body时 ，返回错误:StatusCode: UnsupportedMediaType, Content-Type: application/problem+json; charset=utf-8, Content-Length: -1
            var response = client.Post<AppDTO>(request);
            return await Task.FromResult(response.Data);
        }

        /// <summary>
        /// Post ([FromBody]AppDTO dto)
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp5()
        {
            AppDTO param = new AppDTO()
            {
                UserName = "Post body 3请求",
                Age = 1
            };
            var response=RestClientHelper.PostJson<AppDTO>("http://localhost:5009/api/RestApi/PostAppBody", param);
            return await Task.FromResult(response);
        }

        #region  使用IRestClientService

        /// <summary>
        /// Post ([FromBody]AppDTO dto)
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp6()
        {
            AppDTO param = new AppDTO()
            {
                UserName = "Post body 6请求",
                Age = 1
            };
            var response = _restClientService.PostJson<AppDTO>("http://localhost:5009/api/RestApi/PostAppBody", param);
            return await Task.FromResult(response);
        }

        /// <summary>
        /// Post ([FromQuery]AppDTO dto)
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostApp7()
        {
            var response = _restClientService.PostQuery<AppDTO>("http://localhost:5009/api/RestApi/PostAppUrl", new Dictionary<string, string>() { { "UserName", "高级传参" },{ "Age","12" } });
            return await Task.FromResult(response);
        }


        /// <summary>
        /// Get ([FromQuery]AppDTO dto)
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> GetApp8()
        {
            var response = _restClientService.Get<AppDTO>("http://localhost:5009/api/RestApi/GetApp", new Dictionary<string, string>() { { "UserName", "Query传参" }, { "Age", "485" } });
            return await Task.FromResult(response);
        }

        #endregion

        /// <summary>
        /// get ,(string userName,int age)
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> GetAppQuery()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/GetAppQuery");
            var request = new RestRequest();
            request.AddParameter("UserName", "高级传参");
            request.AddParameter("Age", 12);
            // from body时 ，返回错误:StatusCode: UnsupportedMediaType, Content-Type: application/problem+json; charset=utf-8, Content-Length: -1
            var response = client.Get<AppDTO>(request);
            return await Task.FromResult(response.Data);
        }

        /// <summary>
        /// get [FromQuery]AppDTO dto
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> GetAppUrl()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/GetApp");
            var request = new RestRequest();
            request.AddParameter("UserName", "高级传参");
            request.AddParameter("Age", 12);
            // from body时 ，返回错误:StatusCode: UnsupportedMediaType, Content-Type: application/problem+json; charset=utf-8, Content-Length: -1
            var response = client.Get<AppDTO>(request);
            return await Task.FromResult(response.Data);
        }


        /// <summary>
        /// get [FromQuery]AppDTO dto
        /// </summary>
        /// <returns></returns>
        public async Task<AppDTO> PostAppBodyNoTag()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/PostAppBodyNoTag");
            var request = new RestRequest();
            AppDTO param = new AppDTO()
            {
                UserName = "Post body 3请求",
                Age = 1
            };
            request.AddJsonBody(param);
            var response = client.Post<AppDTO>(request);
            return await Task.FromResult(response.Data);
        }

        /// <summary>
        /// get [FromBody]UserDTO dto
        /// </summary>
        /// <returns></returns>
        public async Task<UserDTO> PostUserBody()
        {
            var client = new RestClient("http://localhost:5009/api/RestApi/PostUserBody");
            client.UseJson();
            var request = new RestRequest();
            UserDTO param = new UserDTO()
            {
                UserName = "Post body 3请求",
                Amount = 10,
                UserIds = new List<string> { "3213", "698" }
            };
            request.AddJsonBody(param);
            var response = client.Post<UserDTO>(request);
            return await Task.FromResult(response.Data);
        }
    }
}