﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace RestSharpClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseUrls("http://*:8083")
            .UseKestrel()
            .UseIISIntegration()
            .UseStartup<Startup>()
            .ConfigureLogging(logging=> {
                ////清理控制台，Debug,Info
                //logging.ClearProviders();
                ////用于输出到程序的控制台
                //logging.AddConsole();
                ////用于输出到Visual Studio 的[输出]窗口（输出来源调试）
                //logging.AddDebug();
                System.Console.WriteLine("ConfigureLogging启动");
                System.Console.WriteLine("http://*:8083");
            });
    }
}
