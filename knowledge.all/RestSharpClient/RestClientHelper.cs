﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RestSharpClient
{
    public static class RestClientHelper
    {

        public static RestClient GetRestClient(string url)
        {
            return new RestClient(url);
        }

        /// <summary>
        /// Post请求,默认或者参数示例([FormBody] AppDTO dto),使用JsonConvert反序列化参数，T类支持广泛
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static T PostJsonAdvanced<T>(string url, object request) where T : new()
        {
            var restRequest = new RestRequest();
            restRequest.AddJsonBody(request);
            var response = GetRestClient(url).Post(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        /// Post请求,默认或者参数示例([FormBody] AppDTO dto),返回参数成员仅支持(注意不支持Array)：Primitives, Decimal,DateTime,String,Guid,List,Dictionary<T1, T2>,Nested classes,Nullable 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static T PostJson<T>(string url, object request) where T : new()
        {
            var restRequest = new RestRequest();
            restRequest.AddJsonBody(request);
            var response = GetRestClient(url).Post<T>(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        ///  Post请求,参数实体示例:([FormQuery] AppDTO dto) 返回参数成员仅支持(注意不支持Array)：Primitives, Decimal,DateTime,String,Guid,List,Dictionary<T1, T2>,Nested classes,Nullable 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static T PostQuery<T>(string url, Dictionary<string, string> request) where T : new()
        {
            var restRequest = new RestRequest();
            if (request != null && request.Any())
            {
                foreach (var item in request)
                {
                    restRequest.AddQueryParameter(item.Key, item.Value);
                }
            }
            var response = GetRestClient(url).Post<T>(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        ///  Post请求,参数实体示例:([FormQuery] AppDTO dto),使用JsonConvert反序列化参数，T类支持广泛
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static T PostQueryAdvanced<T>(string url, Dictionary<string, string> request) where T : new()
        {
            var restRequest = new RestRequest();
            if (request != null && request.Any())
            {
                foreach (var item in request)
                {
                    restRequest.AddQueryParameter(item.Key, item.Value);
                }
            }
            var response = GetRestClient(url).Post(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        ///  Get请求,Api参数格式示例(string userName,int age)或者示例([FromQuery]AppDTO dto) 返回参数成员仅支持(注意不支持Array)：Primitives, Decimal,DateTime,String,Guid,List,Dictionary<T1, T2>,Nested classes,Nullable 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static T Get<T>(string url, Dictionary<string, string> request) where T : new()
        {
            var restRequest = new RestRequest();
            if (request != null && request.Any())
            {
                foreach (var item in request)
                {
                    restRequest.AddParameter(item.Key, item.Value);
                }
            }
            var response = GetRestClient(url).Get<T>(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        ///  Get请求,Api参数格式示例(string userName,int age)或者示例([FromQuery]AppDTO dto)，使用JsonConvert反序列化参数，T类支持广泛
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static T GetAdvanced<T>(string url, Dictionary<string, string> request) where T : new()
        {
            var restRequest = new RestRequest();
            if (request != null && request.Any())
            {
                foreach (var item in request)
                {
                    restRequest.AddParameter(item.Key, item.Value);
                }
            }
            var response = GetRestClient(url).Get(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        /// Post请求,默认或者参数示例([FormBody] AppDTO dto),返回报文
        /// </summary>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string Post(string url, object request)
        {
            var restRequest = new RestRequest();
            restRequest.AddJsonBody(request);
            var response = GetRestClient(url).Post(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }
            else
            {
                throw response.ErrorException;
            }
        }

        /// <summary>
        /// Get请求,Api参数格式示例(string userName,int age)或者示例([FromQuery]AppDTO dto) ，返回报文
        /// </summary>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string Get(string url, Dictionary<string, string> request)
        {
            var restRequest = new RestRequest();
            if (request != null && request.Any())
            {
                foreach (var item in request)
                {
                    restRequest.AddParameter(item.Key, item.Value);
                }
            }
            var response = GetRestClient(url).Get(restRequest);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }
            else
            {
                throw response.ErrorException;
            }
        }
    }
}
