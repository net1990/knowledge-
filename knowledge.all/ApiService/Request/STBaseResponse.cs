﻿using ProtoBuf;
using ProtoBuf.Meta;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    [ProtoContract]
    [ProtoInclude(1, typeof(TBaseResponse))]
    public class STBaseResponse : TBaseResponse
    {
        //static STBaseResponse()
        //{
        //    //RuntimeTypeModel a = RuntimeTypeModel.Default;
        //    //a[typeof(TBaseResponse)].Add(1, "Code").Add(2, "Message").AddSubType(3, typeof(STBaseResponse));// 这一句也可以写在Animal的static定义里
        //    //a[typeof(STBaseResponse)].Add(4, "Status").Add(5, "Data");
        //}

        //public STBaseResponse()
        //{
        //    RuntimeTypeModel a = RuntimeTypeModel.Default;
        //    a[typeof(TBaseResponse)].Add(1, "Code").Add(2, "Message").AddSubType(3, typeof(STBaseResponse));// 这一句也可以写在Animal的static定义里
        //    a[typeof(STBaseResponse)].Add(4, "Status").Add(5, "Data");
        //}

        /// <summary>
        /// 状态
        /// </summary>
        [ProtoMember(4)]
        public bool Status { get; set; }

        [ProtoMember(5)]
        public string Data { get; set; }
    }
}
