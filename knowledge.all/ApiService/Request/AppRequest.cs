﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    /// <summary>
    /// 错误消息
    /// </summary>
    [ProtoContract]
    public class AppRequest
    {

        [ProtoMember(100)]
        public string Mo { get; set; }

        [ProtoMember(101)]
        public string Ho { get; set; }
    }
}
