﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    [ProtoContract]
    public class FeeBillRequest
    {
        public int? Id { get; set; }

        /// <summary>
        /// 单据
        /// </summary>
        public string BillNo { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 总金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 单据类型
        /// </summary>
        public int BillType { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
    }
}
