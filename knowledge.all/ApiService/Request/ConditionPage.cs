﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    [ProtoContract]
    public  class ConditionPage<T>:PageEntity
    {
        [ProtoMember(1)]
        public List<T> ResultList { get; set; }
    }
}
