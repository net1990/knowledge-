﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{

    [ProtoContract]
    public class PageEntity
    {
        [ProtoMember(1)]
        public int PageIndex { get; set; }

        [ProtoMember(2)]
        public int PageSize { get; set; }
    }
}
