﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    /// <summary>
    /// App信息
    /// </summary>
    [ProtoContract]
    public class AppResponse<T> : TBaseResponse
    {
        /// <summary>
        /// 状态
        /// </summary>
        [ProtoMember(21)]
        public bool Status { get; set; }

        [ProtoMember(22)]
        public T Data { get; set; }
    }
}
