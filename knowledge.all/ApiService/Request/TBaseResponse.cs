﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    [ProtoContract]
   // [ProtoInclude(1, typeof(STBaseResponse))]
    public class TBaseResponse
    {
        /// <summary>
        /// 状态码
        /// </summary>
        [ProtoMember(1)]
        public int Code { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        [ProtoMember(2)]
        public string Message { get; set; }
    }
}
