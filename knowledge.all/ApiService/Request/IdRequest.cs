﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    [ProtoContract]
    public class IdRequest
    {
        /// <summary>
        /// Id
        /// </summary>
        [ProtoMember(1)]
        public int Id { get; set; }
    }
}
