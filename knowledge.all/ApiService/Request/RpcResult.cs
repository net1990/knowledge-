﻿using Newtonsoft.Json;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Request
{
    [ProtoContract]
    public class RpcResult<T>
    {
        [ProtoMember(1)]
        public string JsonResult { get; set; }

        public T GetResult()
        {
            if (string.IsNullOrEmpty(JsonResult))
                return default(T);
            return JsonConvert.DeserializeObject<T>(JsonResult);
        }
    }
}
