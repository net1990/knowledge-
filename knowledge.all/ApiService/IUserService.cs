﻿using ApiService.Request;
using ApiService.Response;
using KJFrame.Core.Rpc.Protocol;
using KJFrame.Core.Rpc.Protocol.ServiceAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ApiService
{
    [RpcService]
    public interface IUserService:IRpcService
    {
        //Rpc方法的请求参数，返回参数具有class约束
        //Rpc方法类似于：Task<TOut> Fun<TOut>(TIn request CancellationToken token=default) where TRequest : class where TResponse : class
        //所以Rpc方法的请求桉树，返回参数可以是自定义类，也可以是string等类，但不可以是int,bool,float等数据结构

        [RpcMethod]
        Task<BaseResponse> GetBill(IdRequest request, CancellationToken token=default );

        //这是一个返回类型为String的Rpc方法
        [RpcMethod]
        Task<string> GetString(IdRequest request, CancellationToken token = default);

        [RpcMethod]
        Task<AppResponse<AppRequest>> TestFormat(AppResponse<AppRequest> request, CancellationToken token = default);

        [RpcMethod]
        Task<SignleRequest> TestFormat2(SignleRequest request, CancellationToken token = default);

        [RpcMethod]
        Task<STBaseResponse> TestFormat3(STBaseResponse request, CancellationToken token = default);

        [RpcMethod]
        Task<RpcResult<ConditionPage<IdRequest>>> TestFormat4(IdRequest request, CancellationToken token = default);
    }
}
