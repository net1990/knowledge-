﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Response
{
    [ProtoContract]
    public class FeeBillResponse
    {
        [ProtoMember(1)]
        public int? Id { get; set; }

        /// <summary>
        /// 单据
        /// </summary>
        [ProtoMember(2)]
        public string BillNo { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [ProtoMember(3)]
        public string Description { get; set; }

        /// <summary>
        /// 总金额
        /// </summary>
        [ProtoMember(4)]
        public decimal Amount { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [ProtoMember(5)]
        public int Quantity { get; set; }

        /// <summary>
        /// 单据类型
        /// </summary>
        [ProtoMember(6)]
        public int BillType { get; set; }

        [ProtoMember(7)]
        public string BillTypeName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [ProtoMember(8)]
        public DateTime? CreateTime { get; set; }
    }
}
