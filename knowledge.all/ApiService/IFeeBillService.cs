﻿using ApiService.Request;
using ApiService.Response;
using KJFrame.Core.Rpc.Protocol;
using KJFrame.Core.Rpc.Protocol.ServiceAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ApiService
{
    [RpcService]
    public interface IFeeBillService: IRpcService
    {
        /// <summary>
        ///查费用单
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        [RpcMethod]
        Task<FeeBillResponse> GetFeeBillDetail(IdRequest request, CancellationToken token = default);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [RpcMethod]
        Task<BaseResponse> AddFeeBill(FeeBillRequest model, CancellationToken token = default);

    }
}
