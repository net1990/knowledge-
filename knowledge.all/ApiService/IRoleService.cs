﻿using ApiService.Request;
using ApiService.Response;
using KJFrame.Core.Rpc.Protocol;
using KJFrame.Core.Rpc.Protocol.ServiceAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ApiService
{
    [RpcService]
    public interface IRoleService : IRpcService
    {
        [RpcMethod]
        Task<BaseResponse> GetRole(IdRequest request, CancellationToken token = default);
    }
}
