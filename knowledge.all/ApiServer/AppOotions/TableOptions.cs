﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.AppOotions
{
    /// <summary>
    /// 表配置
    /// </summary>
    public class TableOptions
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object DivisionFieldType { get; set; }

        /// <summary>
        /// 默认Min
        /// </summary>
        public string BeginExpression { get; set; }

        /// <summary>
        /// 默认Max
        /// </summary>
        public string EndExpression { get; set; }
    }
}
