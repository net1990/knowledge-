﻿using DotNetCore.CAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace  ApiServer.SubscribeService
{
    public class CapServerService : ICapSubscribe, ICapServerService
    {
        /// <summary>
        /// 这里用做回调订阅
        /// </summary>
        /// <param name="callInfo"></param>
        [CapSubscribe("topic.callbcak2", Group = "callserver2")]
        public void DemoSubscribeCallBack(string callInfo)
        {
            Console.WriteLine($"收到客户端的回调{callInfo}");
        }

        /// <summary>
        /// 使用ICapSubscribe接口订阅订阅
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.demo3", Group = "group3")]
        public void SubscribeDate(string date)
        {
            Console.WriteLine($"group3订阅事务:{date}");
        }
    }

    public interface ICapServerService
    {
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="date"></param>
        void SubscribeDate(string date);

        /// <summary>
        /// 订阅回调topic
        /// </summary>
        /// <param name="callInfo"></param>
        void DemoSubscribeCallBack(string callInfo);
    }
}
