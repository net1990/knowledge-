﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Com.Ctrip.Framework.Apollo;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ApiServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                        .ConfigureAppConfiguration((hostingContext, builder) =>
                        {
                            #region 配置apollo
                            builder.AddApollo(builder.Build().GetSection("apollo"))
                            //添加应用的命名空间
                            //.AddNamespace("TEST1.Demo0509")
                            .AddNamespace("TEST1.Project")
                            //添加Application命名空间
                            .AddDefault();
                            #endregion
                        })
            .UseUrls("http://localhost:5009")
            .UseIISIntegration()
            .UseStartup<Startup>();
    }
}
