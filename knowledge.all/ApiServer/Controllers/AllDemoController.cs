﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiServer.AppOotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
//using Microsoft.Extensions.Logging;
using KJFrame.Core.Log;
using DotNetCore.CAP;
using System.Data.SqlClient;
using Dapper;
using Newtonsoft.Json;

namespace ApiServer.Controllers
{
    /// <summary>
    /// 所有测试
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AllDemoController : ControllerBase
    {

        /// <summary>
        /// 配置
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// 缓存配置
        /// </summary>
        private readonly CachedOptions _options;

        /// <summary>
        /// 缓存配置
        /// </summary>
        private readonly ComplexOptions _complexOptions;

        /// <summary>
        /// 表配置
        /// </summary>
        private readonly TableOptions _tableOptions;

        /// <summary>
        /// 日志
        /// </summary>
        private readonly IKJLogger _logger;

        /// <summary>
        /// Capbus
        /// </summary>
        private readonly ICapPublisher _capBus;

        private readonly NamedCachedOptions _testOptions;
        private readonly NamedCachedOptions _devOptions;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="options"></param>
        /// <param name="complexOptions"></param>
        /// <param name="tableOptions"></param>
        /// <param name="logger"></param>
        /// <param name="capPublisher"></param>
        /// <param name="optionArr"></param>
        public AllDemoController(IConfiguration configuration,
            IOptions<CachedOptions> options,
            IOptionsSnapshot<ComplexOptions> complexOptions,
            IOptionsMonitor<TableOptions> tableOptions,
            IKJLogger logger,
            ICapPublisher capPublisher,
            IOptionsSnapshot<NamedCachedOptions> optionArr)
        {
            _configuration = configuration;
            //读取一次的IOptions配置
            _options = options.Value;
            _logger = logger;
            _capBus = capPublisher;
            _complexOptions = complexOptions.Value;
            _tableOptions = tableOptions.CurrentValue;

            //监控配置的回调测试
            tableOptions.OnChange((temp, info) =>
            {
                Console.WriteLine(JsonConvert.SerializeObject(temp));
            });

            //命名实例配置
            _testOptions = optionArr.Get("Test1");
            _devOptions = optionArr.Get("Test2");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Index()
        {
            return "hello word";
        }

        /// <summary>
        /// Demo
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> Demo()
        {
            ////简单获取apollo的配置
            //var capConnection = _configuration.GetSection("CapConection")?.Value;
            ////输出配置
            //Console.WriteLine("IOptions<CachedOptions>:"+JsonConvert.SerializeObject(_options));
            //Console.WriteLine("IOptionsSnapshot<ComplexOptions>:"+JsonConvert.SerializeObject(_complexOptions));
            //Console.WriteLine("IOptionsMonitor<TableOptions>:"+JsonConvert.SerializeObject(_tableOptions));
            ////日志
            _logger.Info(Guid.NewGuid().ToString(), "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.Error(Guid.NewGuid().ToString(), "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.Debug(Guid.NewGuid().ToString(), "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.Fatal(Guid.NewGuid().ToString(), "首页日志", "首页日志信息", this.GetType().FullName, "Index");
            _logger.Warn(Guid.NewGuid().ToString(), "首页日志", "首页日志信息", this.GetType().FullName, "Index");

            //简单消息推送,如需测试
            //_capBus.PublishAsync("kjframe.demo1", DateTime.Now);

            //带callback的推送，topic.callbcak2由CapServerService.DemoSubscribeCallBack订阅
            //_capBus.PublishAsync("kjframe.demo2", DateTime.Now, "topic.callbcak2");
            //return capConnection;
            return null;
        }

        /// <summary>
        /// 手动提交事务
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DemoTransaction()
        {
            using (var connnection = new SqlConnection("Integrated Security=False;server=192.168.1.109;database=cap;User ID=sa;Password=198603yang;Connect Timeout=30"))
            {
                using (var transaction = connnection.BeginTransaction(_capBus, false))
                {
                    var sql = "update UserTransaction set Name='嘻嘻' where id=1";
                    connnection.Execute(sql, null, transaction);
                    await _capBus.PublishAsync("kjframe.demo3", DateTime.Now);
                    transaction.Commit();
                    return Content("DemoTransaction服务端send手动提交事务");
                }
            }
        }

        /// <summary>
        /// 自动提交事务
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DemoTransactionAuto()
        {
            using (var connnection = new SqlConnection("Integrated Security=False;server=192.168.1.109;database=cap;User ID=sa;Password=198603yang;Connect Timeout=30"))
            {
                using (var transaction = connnection.BeginTransaction(_capBus, true))
                {
                    var sql = "update UserTransaction set Name='hahahah' where id=1";
                    connnection.Execute(sql, null, transaction);
                    await _capBus.PublishAsync("kjframe.demo3", DateTime.Now);
                    return Content("DemoTransactionAuto服务端send自动提交事务");
                }
            }
        }

    }
}