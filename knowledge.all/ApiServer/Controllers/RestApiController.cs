﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiServer.Controllers
{
    /// <summary>
    /// RestSharp 的Api测试
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RestApiController : ControllerBase
    {


        /// <summary>
        /// RestSharp HttpGet 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="age"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<AppDTO> GetAppQuery(string userName,int age)
        {
            return await Task.FromResult(new AppDTO() { Age=age,UserName= userName });
        }

        /// <summary>
        ///  RestSharp HttpGet [FromBodyl]测试
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<AppDTO> GetApp([FromQuery]AppDTO dto)
        {
            return await Task.FromResult(dto);
        }

        /// <summary>
        ///  RestSharp HttpPost [FromUrl]测试
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<AppDTO> PostAppUrl([FromQuery]AppDTO dto)
        {
            return await Task.FromResult(dto);
        }

        /// <summary>
        ///  RestSharp HttpPost [FromBodyl]测试
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<AppDTO> PostAppBody([FromBody]AppDTO dto)
        {
            return await Task.FromResult(dto);
        }

        /// <summary>
        ///  RestSharp 等同于[FromBody]
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<AppDTO> PostAppBodyNoTag(AppDTO dto)
        {
            return await Task.FromResult(dto);
        }

        /// <summary>
        ///  RestSharp 复杂类型 HttpGet [FromBodyl]测试
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<UserDTO> GetUser(UserDTO dto)
        {
            return await Task.FromResult(dto);
        }

        /// <summary>
        ///  RestSharp 复杂类型 HttpGet [FromUrl]测试
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<UserDTO> PostUserUrl([FromQuery]UserDTO dto)
        {
            return await Task.FromResult(dto);
        }


        /// <summary>
        ///  RestSharp 复杂类型 HttpGet [FromBodyl]测试
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<UserDTO> PostUserBody(UserDTO dto)
        {
            return await Task.FromResult(dto);
        }
    }
}