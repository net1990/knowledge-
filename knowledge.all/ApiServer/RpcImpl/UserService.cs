﻿using ApiService;
using ApiService.Request;
using ApiService.Response;
using KJFrame.Core.Rpc.Protocol;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ApiServer.RpcImpl
{
    public class UserService : RpcServiceBase, IUserService
    {
        public Task<BaseResponse> GetBill(IdRequest request, CancellationToken token = default)
        {
            return Task.FromResult(new BaseResponse() { Status=true,Message="50002pp+No"+request.Id });
        }

        public Task<string> GetString(IdRequest request, CancellationToken token = default)
        {
            return Task.FromResult(request.Id.ToString());
        }

        public Task<AppResponse<AppRequest>> TestFormat(AppResponse<AppRequest> request, CancellationToken token = default)
        {
            
            return Task.FromResult(request);
        }

        public Task<SignleRequest> TestFormat2(SignleRequest request, CancellationToken token = default)
        {
            return Task.FromResult(request);
        }

        public Task<STBaseResponse> TestFormat3(STBaseResponse request, CancellationToken token = default)
        {
            return Task.FromResult(request);
        }

        public Task<RpcResult<ConditionPage<IdRequest>>> TestFormat4(IdRequest request, CancellationToken token = default)
        {
            var response = new ConditionPage<IdRequest>() {  PageIndex=12,PageSize=100, ResultList=new System.Collections.Generic.List<IdRequest>() };
            response.ResultList.Add(request);

            var rpc = new RpcResult<ConditionPage<IdRequest>>() { JsonResult=JsonConvert.SerializeObject(response) };
            return Task.FromResult<RpcResult<ConditionPage<IdRequest>>>(rpc);
        }
    }
}
