﻿using KJFrame.Core.Job;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulerJob.job
{
    public class OrderJob : IRepeatJob
    {
        public OrderJob()
        {
            JobName = "OrderJob";
        }

        public string JobName
        {
            get; set;
        }

        public void Excute()
        {
            Console.WriteLine($"执行任务{JobName}:{DateTime.Now}");
        }
    }
}
