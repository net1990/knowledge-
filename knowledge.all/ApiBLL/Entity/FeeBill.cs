﻿using KJFrame.Core.Entity;
using KJFrame.Core.Entity.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kdfafa.Start.FeeBill.BLL.Entity
{
    /// <summary>
    /// FeeBill，费用单
    /// </summary>
    [Serializable]
    [TableMapping(Name = "FeeBill", PrimaryKey = "Id", DefaultOrderBy = "Id desc")]
    public class FeeBill : BaseEntity
    {
        private int? _id;
        private string _billNo;
        private string _description;
        private decimal _amount;
        private int _quantity;
        private int _billType;
        private DateTime? _createTime;

        /// <summary>
        /// 主键,自增
        /// </summary>
        [ColumnMapping(Name = "Id", AutoIncrement = true)]
        public int? Id
        {
            get { return _id; }
            set
            {
                object _oldvalue = _id;
                _id = value;

                if ((value == null) || (!value.Equals(_oldvalue)))
                {
                    this.OnPropertyChanged("Id", _oldvalue, _id);
                }
            }
        }

        /// <summary>
        /// 单据
        /// </summary>
        [ColumnMapping(Name = "BillNo")]
        public string BillNo
        {
            get { return _billNo; }
            set
            {
                object _oldvalue = _billNo;
                _billNo = value;

                if ((value == null) || (!value.Equals(_oldvalue)))
                {
                    this.OnPropertyChanged("BillNo", _oldvalue, _billNo);
                    this.OnMappingPropertyChanged("BillNo", _oldvalue, _billNo);
                }
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        [ColumnMapping(Name = "Description")]
        public string Description
        {
            get { return _description; }
            set
            {
                object _oldvalue = _description;
                _description = value;

                if ((value == null) || (!value.Equals(_oldvalue)))
                {
                    this.OnPropertyChanged("Description", _oldvalue, _description);
                    this.OnMappingPropertyChanged("Description", _oldvalue, _description);
                }
            }
        }

        /// <summary>
        /// 总金额
        /// </summary>
        [ColumnMapping(Name = "Amount")]
        public decimal Amount
        {
            get { return _amount; }
            set
            {
                object _oldvalue = _amount;
                _amount = value;

                if (!value.Equals(_oldvalue))
                {
                    this.OnPropertyChanged("Amount", _oldvalue, _amount);
                    this.OnMappingPropertyChanged("Amount", _oldvalue, _amount);
                }
            }
        }

        /// <summary>
        /// 数量
        /// </summary>
        [ColumnMapping(Name = "Quantity")]
        public int Quantity
        {
            get { return _quantity; }
            set
            {
                object _oldvalue = _quantity;
                _quantity = value;

                if (!value.Equals(_oldvalue))
                {
                    this.OnPropertyChanged("Quantity", _oldvalue, _quantity);
                    this.OnMappingPropertyChanged("Quantity", _oldvalue, _quantity);
                }
            }
        }

        /// <summary>
        /// 单据类型
        /// </summary>
        [ColumnMapping(Name = "BillType")]
        public int BillType
        {
            get { return _billType; }
            set
            {
                object _oldvalue = _billType;
                _billType = value;

                if (!value.Equals(_oldvalue))
                {
                    this.OnPropertyChanged("BillType", _oldvalue, _billType);
                    this.OnMappingPropertyChanged("BillType", _oldvalue, _billType);
                }
            }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [ColumnMapping(Name = "CreateTime")]
        public DateTime? CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                object _oldvalue = _createTime;
                _createTime = value;

                if ((value == null) || (!value.Equals(_oldvalue)))
                {
                    this.OnPropertyChanged("CreateTime", _oldvalue, _createTime);
                    this.OnMappingPropertyChanged("CreateTime", _oldvalue, _createTime);
                }
            }
        }
    }
}
