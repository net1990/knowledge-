﻿using ApiService;
using ApiService.Request;
using ApiService.Response;
using KJFrame.Core.BLL;
using KJFrame.Core.Entity.ExpressionClips;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kdfafa.Start.FeeBill.BLL.Service
{
    public class FeeBillService : BaseBLLRpc, IFeeBillService
    {

        public Task<BaseResponse> AddFeeBill(FeeBillRequest model, CancellationToken token = default)
        {
            if (model == null)
                throw new Exception("参数为空");
            Start.FeeBill.BLL.Entity.FeeBill entity = new Start.FeeBill.BLL.Entity.FeeBill()
            {
                CreateTime = DateTime.Now,
                Amount = model.Amount,
                Quantity = model.Quantity,
                Description = model.Description,
                BillType = model.BillType,
                BillNo = DateTime.Now.ToString("yyyyMMddhhmmssfff")
            };
            bool flag = this.Add(entity) > 0;
            return Task.FromResult<BaseResponse>(new BaseResponse() { Status = flag });
        }

        public Task<FeeBillResponse> GetFeeBillDetail(IdRequest request, CancellationToken token = default)
        {
            if (request.Id <= 0)
                return null;
            var entity = this.Get<Start.FeeBill.BLL.Entity.FeeBill>(new WhereClip<Start.FeeBill.BLL.Entity.FeeBill>(a => a.Id == request.Id));
            if (entity != null)
            {
                return Task.FromResult(new FeeBillResponse()
                {
                    Id = entity.Id,
                    BillNo = entity.BillNo,
                    Amount = entity.Amount,
                    BillType = entity.BillType,
                    Description = entity.Description,
                    CreateTime = entity.CreateTime,
                    Quantity = entity.Quantity
                }
                );
            }
            return null;
        }
    }
}
