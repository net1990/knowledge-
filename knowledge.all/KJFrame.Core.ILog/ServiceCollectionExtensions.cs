﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace KJFrame.Core.ILog
{
    public static class ServiceCollectionExtensions
    {
        //public static IServiceCollection UseLog4net(this IServiceCollection services,string configFile)
        //{
        //    services.AddSingleton<ILogger>(serviceProvider =>
        //    {

        //    });
        //    return services;
        //}

        public static void UseLog4net(this IServiceProvider provider, string configFile)
        {
            provider.GetRequiredService<ILoggerFactory>().AddProvider(new Log4NetProvider(configFile));
        }

        
    }
}
