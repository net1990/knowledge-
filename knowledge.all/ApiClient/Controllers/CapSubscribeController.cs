﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;

namespace ApiClient.Controllers
{
    /// <summary>
    /// Cap客户端订阅
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CapSubscribeController : ControllerBase
    {

        /// <summary>
        /// group1订阅组
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.demo1", Group = "group1")]
        [NonAction]
        public void SubscribeGroup2(string date)
        {
            Console.WriteLine($"group1接收到消息:{date}");
        }

        /// <summary>
        /// group2订阅组
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.demo1", Group = "group2")]
        [NonAction]
        public void SubscribeGroup1(string date)
        {
            Console.WriteLine($"group2接收到消息:{date}");
        }

        /// <summary>
        /// group4订阅组
        /// </summary>
        /// <param name="date"></param>
        [CapSubscribe("kjframe.demo2", Group = "group4")]
        [NonAction]
        public DateTime SubscribeGroup4(DateTime date)
        {
            Console.WriteLine($"group4已处理，请回调:{date.ToString("yyyy-MM-dd hh:mm:ss")}");
            return DateTime.Now.AddDays(10);
        }
    }
}
