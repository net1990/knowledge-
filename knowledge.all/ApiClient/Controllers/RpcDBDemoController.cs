﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiService;
using ApiService.Request;
using ApiService.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiClient.Controllers
{
    /// <summary>
    /// 使用Rpc调用服务端访问数据库
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RpcDBDemoController : ControllerBase
    {
        //测试访问数据
        private readonly IFeeBillService _feeBillService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="feeBillService"></param>
        public RpcDBDemoController(IFeeBillService feeBillService)
        {
            _feeBillService = feeBillService;
        }

        /// <summary>
        /// 查询费用单据
        /// </summary>
        /// <param name="id">费用单据ID</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<FeeBillResponse> GetFeeBill(int id)
        {
            var dto= await _feeBillService.GetFeeBillDetail(new IdRequest() { Id = id });
            return dto;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<BaseResponse> AddFeeBill(FeeBillRequest request)
        {
            var flag = _feeBillService.AddFeeBill(request).Result;
            return flag;
        }
    }
}