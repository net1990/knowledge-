﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ApiService;
using ApiService.Request;
using ApiService.Response;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiClient.Controllers
{
    /// <summary>
    /// Rpc 客户端 Demo
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RpcClientDemoController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="roleService"></param>
        public RpcClientDemoController(IUserService userService, IRoleService roleService)
        {
            this._userService = userService;
            this._roleService = roleService;
        }

        /// <summary>
        /// Rpc简单使用
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<BaseResponse> GetBill(int id)
        {
            //客户端为5002(UseUrls("http://localhost:5002"))
            //此处请求的是服务端5001的数据
            return _userService.GetBill(new ApiService.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
        }

        /// <summary>
        /// Rpc返回string类型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<String> GetString(int id)
        {
            return _userService.GetString(new ApiService.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
        }

        /// <summary>
        /// Rpc返回string类型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<String> GetDemoFormat()
        {
            var request = new AppResponse<AppRequest>() {  Code=100, Message="你好啊", Status=true,Data=new AppRequest() {  Mo="momomo",Ho="hiohihdisa"} };
            var json= _userService.TestFormat(request, new CancellationTokenSource(1000 * 10).Token).Result;
            return JsonConvert.SerializeObject(json);
        }


        /// <summary>
        /// Rpc返回string类型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<String> GetDemoFormat2()
        {
            var request = new SignleRequest() { Code = 100, Message = "你好啊", Status = true, Data = new AppRequest() { Mo = "momomo", Ho = "hiohihdisa" } };
            var json = _userService.TestFormat2(request, new CancellationTokenSource(1000 * 10).Token).Result;
            return JsonConvert.SerializeObject(json);
        }

        /// <summary>
        /// Rpc返回string类型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<String> GetDemoFormat3()
        {
            var request = new STBaseResponse() { Code = 100, Message = "你好啊", Status = true, Data = "BNFDASDK"};
            var json = _userService.TestFormat3(request, new CancellationTokenSource(1000 * 10).Token).Result;
            return JsonConvert.SerializeObject(json);
        }


        /// <summary>
        /// Rpc返回string类型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<String> GetDemoFormat4()
        {
            var request = new IdRequest() { Id=9312378};
            var json = _userService.TestFormat4(request, new CancellationTokenSource(1000 * 10).Token).Result;
            var response = json.GetResult();
            return JsonConvert.SerializeObject(json);
        }

        /// <summary>
        /// Rpc查角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<BaseResponse> GetRole(int id)
        {
            //客户端为5002(UseUrls("http://localhost:5002"))
            //此处请求的是服务端5001的数据
            return _roleService.GetRole(new ApiService.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> index()
        {
            return "hello word";
        }

    }
}
