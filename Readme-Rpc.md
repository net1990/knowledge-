# gRpc

**Rpc**

Rpc是一种建立在Tcp传输层上的，用于客户端与服务端之间的服务调用，远程过程调用；和Http，WCF,WebService一样，都用于客户端与服务端之间的服务调用，区别在于他们之间的实现方式的差异

## Rpc与Http

Http：应用层，报文较大，传输相对慢，应用广泛，通用性好，安全机制完善，客户端支持丰富，多用于系统服务对外开放

Rpc：在Tcp传输层协议上的封装通信，具有更好的通信效率；主要用于内部系统之间的服务相互访问

## Rpc的调用流程

*RPC框架的目标就是要2~8这些步骤都封装起来，让用户对这些细节透明*

> 一次完整的RPC调用流程（同步调用，异步另说）如下：

>1）服务消费方（client）调用:以本地调用方式调用服务；

>2）client stub接收到调用后负责将方法、参数等组装成能够进行网络传输的消息体；

>3）client stub找到服务地址，并将消息发送到服务端；

>4）server stub收到消息后进行解码；

>5）server stub根据解码结果调用服务端的本地的服务；

>6）服务端的本地服务执行并将结果返回给server stub；

>7）server stub将返回结果打包成消息并发送至消费方；

>8）client stub接收到消息，并进行解码；

>9）服务消费方得到最终结果。

## gRpc

gRPC是Google的开源的RPC框架，基于最新的HTTP2.0协议，并支持常见的众多编程语言。

## KJFrame.Core.Grpc

基于gRpc的封装，在Grpc，Grpc.HealthCheck，protobuf-net上封装的grpc框架

包括KJFrame.Core.Rpc.Server(服务端)，KJFrame.Core.Rpc.Client(客户端)，KJFrame.Core.Rpc.Protocol(协议)三个组件

# 使用

# 服务端教程

1)服务端引入程序集

```c#
    using KJFrame.Core.Rpc.Protocol;
    using KJFrame.Core.Rpc.Server;
```
# 从KJFrame.Core.Rpc.Protocol 1.0.5开始支持JsonSerializer序列化，由于buf对于c#的继承支持能力太差，建议使用Json序列化

# 当您使用Json序列化时，可以不使用[ProtoContract]，[ProtoMember]用于标记协议的数据定义

2)服务端配置

```c#
        public void ConfigureServices(IServiceCollection services)
        {
            //添加buf序列化的Rpc服务器服务
            //services.AddRpcServer(option => {
            //"RpcServer"为Rpc在appsettings.json中的配置节点，包括端口号
            //    Configuration.GetSection("RpcServer").Bind(option);
            //});
            
            //使用json序列化的Rpc服务器
            services.AddRpcServer<JsonSerializer>(option =>
            {
                Configuration.GetSection(RpcConfigInformation.RpcServerConfigSectionName).Bind(option);
            });
			
		//注册服务器端开放给客户端的服务
		//以下注释即公开了一个继承自IRPCService,具有[RpcService]特性的RPC服务
            //services.AddScoped<IUserService, UserService>();
        }
```

3)服务端appsettings.json配置Rpc服务器信息

```json        
        {
            "RpcServer": {
                "Port": 12003,
                "EnableServiceDiscovery": false
            }
        }
```

4)在服务端定义一个Rpc服务

```c#	
        //定义一个ProtoBuf协议的Rpc请求参数
        //使用[ProtoContract]特性标记其为ProtoBuf协议参数
        
        using ProtoBuf;
	    
        [ProtoContract]
        public class IdRequest
        {
            [ProtoMember(1)]
            public int Id { get; set; }
        }
        
        //定义一个ProtoBuf协议的Rpc返回参数
        //使用[ProtoContract]特性标记其为ProtoBuf协议参数
        using ProtoBuf;
        
        [ProtoContract]
        public  class BaseResponse
        {
            // 状态
            [ProtoMember(1)]
            public bool Status { get; set; }
            
            // 消息
            [ProtoMember(2)]
            public string Message { get; set; }
        }
        //定义一个Rpc服务，使用[RpcService]特性标记RPC服务，并继承IRpcService空接口
        //需要公开的Rpc方法以[RpcMethod]特性标记
        //Rpc方法必须要有两个参数，第二个参数必须是CancellationToken token
        //请求参数与返回参数具有：class约束
        //即请求方法类似于:Task<TOut> Fun<TOut>(TIn request CancellationToken token=default) where TRequest : class where TResponse : class;
        
        using KJFrame.Core.Rpc.Protocol;
        using KJFrame.Core.Rpc.Protocol.ServiceAnnotations;
        
        [RpcService]
        public interface IUserService:IRpcService
        {
            [RpcMethod]
            Task<BaseResponse> GetBill(IdRequest request, CancellationToken token=default );
        }
```

5)在服务端定义实现RPC服务接口(示例，实现上文的IUserService)

```c#
        //需要继承KJFrame.Core.Rpc.Server.RpcServiceBase类
    
        public class UserService : RpcServiceBase, IUserService
        {
            public Task<BaseResponse> GetBill(IdRequest request, CancellationToken token = default)
            {
                return Task.FromResult(new BaseResponse() { Status=true,Message="50002pp+No"+request.Id });
            }
        }
```

# 客户端教程

1)客户端引入程序集

```c#
    using KJFrame.Core.Rpc.Client;
```
2)客户端配置

```c#
        public void ConfigureServices(IServiceCollection services)
        {
            //省略其他代码
            
            //以下AddRpcClient扩展添加了一个buf序列化的Rpc客户端，此方法会加载"RpcClient"这个配置节点用于存根Rpc服务器的信息
            //此处AddRpcClientService扩展方法向Rpc客户端注册了一个Rpc服务
            
            //services.AddRpcClient(Configuration)
            //        .AddRpcClientService<IUserService>()
            //        .BuildServiceProvider();
            
            //下面注释使用的是AddRpcClientService，批量将所有继承于IRpcService接口的RpcService服务注入
            
            //services
            //      .AddRpcClient(Configuration)
            //      .AddRpcClientService()
            //      .BuildServiceProvider();
            
            //以下注释用于注册一个Json序列化的Rpc客户端，并批量将所有继承于IRpcService接口的RpcService服务注入
            services.AddRpcClient<JsonSerializer>(Configuration).AddRpcClientService().BuildServiceProvider();
                    
            //省略其他代码
        }
```        
        
3)客户端appsettings.json配置Rpc服务器信息

```json       
        //这里的Host是Rpc服务器的地址，Port的是Rpc服务器的端口
        
        {
            "RpcServer": {
                "Host": "127.0.0.1",
                "Port": 12003,
                "EnableServiceDiscovery": false
            }
        }
```

4)客户端调用服务端的Rpc服务

```c#
        //这里示范了在.net core api controller客户端是如何调用Rpc服务
        //示例中，客户端只需要定义或者引入继承了IRpcService接口的IUserService Rpc服务，
        //而无需引入或者定义IUserService的实现类（UserService）
        
        [Route("api/[controller]/[action]")]
        [ApiController]
        public class ValuesController : ControllerBase
        {
            private readonly IUserService _userService;

            public ValuesController(IUserService userService)
            {
                this._userService = userService;
            }

            [HttpGet]
            public string Index()
            {
                return "Hello Word!";
            }

            [HttpGet]
            public ActionResult<BaseResponse> Get(int id)
            {
        
                //此处请求的是RPC服务端的数据,RpcClient将会由本地代理类向Rpc服务器发送tcp请求数据
                return _userService.GetBill(new DemoProtocol.Request.IdRequest() { Id = id }, new CancellationTokenSource(1000 * 10).Token).Result;
            }
        }
```

5)Rpc请求Context(在Rpc方法中，你可以通过Context访问请求的相关信息)

```c#
        public class RoleService : RpcServiceBase, IRoleService
        {
            public Task<BaseResponse> GetRole(IdRequest request, CancellationToken token = default)
            {
                var context = new
                {
                    //过期时间：9999-12-31T23:59:59.9999999
                    Deadline = Context.Deadline,
                    //服务器：127.0.0.1:12003
                    Host = Context.Host,
                    //请求的路径： /IRoleService/GetRole
                    Method = Context.Method,
                    //发起请求的客户端：ipv4:127.0.0.1:54792
                    Peer = Context.Peer,
                    //请求头：[{"Key":"user-agent",
                    //             "ValueBytes":"Z3JwYy1jc2hhcnAvMS4yMS4wIGdycGMtYy83LjAuMCAod2luZG93czsgY2h0dHAyOyBnYW5kYWxmKQ==",
                    //            "Value":"grpc-csharp/1.21.0 grpc-c/7.0.0 (windows; chttp2; gandalf)","IsBinary":false}]
                    RequestHeaders = Context.RequestHeaders,
                    //状态：{"StatusCode":0,"Detail":""}
                    Status = Context.Status,
                    Trailers = Context.ResponseTrailers,
                    UserState = Context.UserState,
                    GetRole = request.Id
                };
                return Task.FromResult(new BaseResponse() { Status = false, Message = JsonConvert.SerializeObject(context) });
            }
        }
```

```json
        //上文context变量的样文数据
        {
                "Deadline": "9999-12-31T23:59:59.9999999", 
                "Host": "127.0.0.1:12003", 
                "Method": "/IRoleService/GetRole", 
                "Peer": "ipv4:127.0.0.1:54792", 
                "RequestHeaders": [
                    {
                        "Key": "user-agent", 
                        "ValueBytes": "Z3JwYy1jc2hhcnAvMS4yMS4wIGdycGMtYy83LjAuMCAod2luZG93czsgY2h0dHAyOyBnYW5kYWxmKQ==", 
                        "Value": "grpc-csharp/1.21.0 grpc-c/7.0.0 (windows; chttp2; gandalf)", 
                        "IsBinary": false
                    }
                ], 
                "Status": {
                    "StatusCode": 0, 
                    "Detail": ""
                }, 
                "Trailers": [ ], 
                "UserState": { }, 
                "GetRole": 10000
        }
```

5)Rpc客户端只是有一个Rpc服务接口，而无接口的实现类，最终是请求服务器端，执行服务器端的接口的实现类