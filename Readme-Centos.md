# .NET Core程序在 Centos上的部署

# 教程前提

    CentOs7.6 64位(VMWare虚拟机)
    .NET Core2.2
    XFTP工具
    PUTTY工具
# 参考文献：CentOS安装.net core SDK进入下面连接，在Linux Distribution下拉框中后选择CentOS / Oracle - x64
    
    https://dotnet.microsoft.com/download/linux-package-manager/centos/sdk-current
    
# 提示(新手此处可能被坑)

#### 1，Linux的文件系统大小写敏感，在部署中注意文件，文件夹的大小写问题

#### 2，.NET Core部署Linux时上时，注意代码中使用：UseKestrel()；不要使用UseIIS();

```c#
     {
         //省略了其他代码
         WebHost.CreateDefaultBuilder(args)
            .UseUrls($"http://*:{port}")
            .UseKestrel() //UseIIS();
            .UseStartup<Startup>()
        //省略了其他代码
     }
```

# CentOS安装.NET Core2.2 SDK

#### 1，安装微软密钥

```c#
    sudo rpm -Uvh https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm
```
#### 2，安装.NET Core 2.2 SDK

```c#
    sudo yum update
    sudo yum install dotnet-sdk-2.2
```

#### 3，验证安装；输入dotnet --version，出现版本信息即安装成功

# 打包.NET Core程序

#### 1，在VS中选择项目，右键选择发布

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/publish1.png "markdown")

#### 2，选择文件夹发布，发布路径默认为publish(推荐),注意摘要中的配置是Release的
    
    如果您不小心打包到的是Debug配置，那么部署时读取到的配置将会是appsettings.Development.json，而不是用于生产的appsettings.json

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/publish2.png "markdown")

#### 3，点击发布

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/publish3.png "markdown")

#### 4，然后将publish中的文件打包
   
#### 5，本样例打包的注意事项
   建议将pdb格式的文件删除
   
   如果是第一次部署，注意配置文件appsettings.json中的端口，apollo之类的配置
   
   如果是第一次部署，注意Log4net的配置文件夹Config(首字母大写，这是个坑)
   
   为了便于日志收集，注意Log4net的配置文件log4net.config中的File参数值应配置成:/data/logs/Kdfafa.Agent.Api（项目名称）/info.log
   
   如果是更新发版，推荐不要把Config文件夹，appsettings.json打包上去

# 部署到CentOS

#### 1，使用XFTP工具连接CentOS，并在var文件夹下创建www文件夹(有则忽略)

#### 2，在www文件夹下创建自己的项目部署文件夹，本例(/var/www/agent)

#### 3，将打包好的publish下的文件通过XFTP上传到部署文件夹（agent），并解压；或者您也可以直接把publish下的文件上传到agent下
    
    3.1关于解压.zip（注意解压的路径，对新手这可能被坑）
    
    安装.zip格式解压软件命令：sudo yum install -y unzip zip
    解压名为code.zip的压缩包命令（此处该命令解压到当前目录下的code文件夹中）：unzip code.zip
    更详细的解压命令请自己找，尤其是解压到指定目录

#### 4，无论你是采用直接复制也好，还是压缩包解压也好，样例中最后部署的文件(那些dll)是放在了/var/www/agent/文件夹下,如下图

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/publish4.png "markdown")

#### 5，Host映射

  如果您的数据库，Apollo，Redis,Kafka等服务器是使用域名访问的，您可能需要配置Host文件的映射，如果不需要，请忽略此步骤
  
  CentOS7.6的Host映射类似于Windows的Host配置映射，CentOS7.6的Host映射配置在这个文件中/etc/hosts配置；配置方式，请网上找教程（直接搜索/etc/hosts）

#### 6，打开端口（首次部署，如果没打开端口，外网无法访问你部署在CentOS7.6上的站点服务)
  
  如果您的.NET Core项目使用到了相关端口，请检查端口是否打开，如果没有，请注意打开或者关闭防火墙

```c#  
  CentOS7.6检查打开的端口
  firewall-cmd  --list-port
  
  CentOS7.6打开一个端口（以8080端口为例）
  firewall-cmd  --add-port=8080/tcp --permanent
  
  CentOS7.6重新加载防火墙
  firewall-cmd  --reload
  
  赠送如何开启防火墙状态
  systemctl start firewalld
  
  赠送如何关闭防火墙
  systemctl stop firewalld
  
  赠送如何查看防火墙状态
  systemctl status firewalld
```

#### 7，启动站点服务

```c# 
 
 //直接启动使用： dotnet /var/www/agent/Agent.dll
 //注意：这里有个坑，如果使用了Apollo，请cd /var/www/agent/目录下，再执行dotnet Agent.dll
 //建议按下面两种方法执行
 
 
 //cd到项目部署目录下（本例/var/www/agent/）
 
 cd /var/www/agent/
 
 //运行站点并查看控制台(此方法适合查看程序是否能够正常启动，查看启动时的控制台输出，但是无法切换界面，按Ctrl+c会关闭程序)
 
 dotnet Agent.dll
 
 //以后台进程运行站点
 
 nohup dotnet Agent.dll & jobs -l
 
```

#### 8，关闭后台运行的dotnet core程序

```c# 
    //使用grep查看后台dotnet core程序
    
    ps -ef|grep dotnet
    
    //关闭进程，将您想要关闭的dotnet core进程关闭（-9表示强制）
    //kill -9 进程ID；
      
    kill -9 进程ID
```
    如下图

![markdown](https://gitlab.com/net1990/knowledge-/raw/master/docs/images/publish5.png "markdown")
    

